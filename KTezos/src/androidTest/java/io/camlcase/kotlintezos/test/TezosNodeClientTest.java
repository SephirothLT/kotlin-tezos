/**
 * # Released under MIT License
 * <p>
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import io.camlcase.kotlintezos.NetworkClient;
import io.camlcase.kotlintezos.TezosNodeClient;
import io.camlcase.kotlintezos.data.RPC;
import io.camlcase.kotlintezos.model.TezosCallback;
import io.camlcase.kotlintezos.operation.ForgingService;
import io.camlcase.kotlintezos.operation.forge.ForgingVerifier;
import io.camlcase.kotlintezos.operation.forge.RemoteForgingService;
import io.camlcase.kotlintezos.operation.forge.RemoteForgingVerifier;
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService;
import io.camlcase.kotlintezos.operation.metadata.MetadataService;
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor;

/**
 * Creation tests for {@link TezosNodeClient}
 */
public class TezosNodeClientTest {

    @Test
    public void testCreationDefaultContructor() {
        TezosNodeClient client = TezosNodeClient.Companion.invoke("http://localhost.com", "http://localhost2.com", true);
        Assert.assertNotNull(client);
    }

    @Test
    public void testCreationPrimaryContructor() {
        NetworkClient javaImplementationMockClient = new NetworkClient() {
            public <T> void send(@NotNull RPC<T> rpc, @NotNull final TezosCallback<T> callback) {
                // Nothing to do
            }

            @NotNull
            public <T> CompletableFuture<T> send(@NotNull RPC<T> rpc) {
                CompletableFuture<T> future = new CompletableFuture<>();
                future.complete(null);
                return future;
            }
        };

        CurrentThreadExecutor executorService = new CurrentThreadExecutor();
        // In reality, the ForgingVerifier should be created with a different NetworkClient with separate base url
        ForgingVerifier verifier = new RemoteForgingVerifier(javaImplementationMockClient, executorService);
        RemoteForgingService remoteForgingService = new RemoteForgingService(javaImplementationMockClient, verifier, executorService);
        ForgingService service = new ForgingService(remoteForgingService, executorService);

        TezosNodeClient client = initClient(javaImplementationMockClient);
        Assert.assertNotNull(client);
    }

    public static TezosNodeClient initClient(NetworkClient mockClient) {
        CurrentThreadExecutor executorService = new CurrentThreadExecutor();
        // In reality, the ForgingVerifier should be created with a different NetworkClient with separate base url
        ForgingVerifier verifier = new RemoteForgingVerifier(mockClient, executorService);
        return initClient(mockClient, verifier);
    }

    public static TezosNodeClient initClient(NetworkClient mockClient, ForgingVerifier verifier) {
        CurrentThreadExecutor executorService = new CurrentThreadExecutor();
        RemoteForgingService remoteForgingService = new RemoteForgingService(mockClient, verifier, executorService);
        ForgingService service = new ForgingService(remoteForgingService, executorService);
        MetadataService metadataService = new BlockchainMetadataService(mockClient, executorService);
        return new TezosNodeClient(mockClient, executorService, metadataService, service);
    }

}
