/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SecretKey
import org.junit.Assert
import org.junit.Test

internal class PublicKeyTest {
    @Test
    fun testBase58CheckRepresentation() {
        val publicKey = PublicKey(secretKey = secretKey)
        Assert.assertEquals(publicKey!!.base58Representation, EXPECTED_PUBLIC_KEY)
    }

    @Test
    fun testPublicKeyHash() {
        val publicKey = PublicKey(secretKey = secretKey)
        Assert.assertEquals("tz1Y3qqTg9HdrzZGbEjiCPmwuZ7fWVxpPtRw", publicKey!!.hash)
    }

    @Test
    fun testVerifyHex() {
        val hexToSign = "123456"
        val secretKey2 =
            SecretKey("e5685a961eb8570b3d078e589c59920c7596f60bde6806f53e269e8cecabe34b317589a652e86e859ce824329b408198950b8d9f2d6bd460085907b803d7be14")
        Assert.assertNotNull(secretKey)
        Assert.assertNotNull(secretKey2)

        val publicKey1 = PublicKey(secretKey = secretKey)
        val publicKey2 = PublicKey(secretKey = secretKey2)

        val signature: ByteArray? = secretKey?.sign(hexToSign)
        Assert.assertTrue(publicKey1?.verify(signature!!, hexToSign)!!)
        Assert.assertFalse(publicKey2?.verify(signature!!, hexToSign)!!)
        Assert.assertFalse(publicKey1.verify(byteArrayOf(1, 2, 3), hexToSign))
    }

    companion object {
        private val secretKeyHex = "cce78b57ed8f4ec6767ed35f3aa41df525a03455e24bcc45a8518f63fbeda772429a986c8072a40a1f3a3e2ab5a5819bb1b2fb69993c5004837815b9dc55923e"
        private val secretKey = SecretKey(secretKeyHex)
        private const val EXPECTED_PUBLIC_KEY = "edpku9ZF6UUAEo1AL3NWy1oxHLL6AfQcGYwA5hFKrEKVHMT3Xx889A"
    }
}
