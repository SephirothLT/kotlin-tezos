/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.ConseilClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.conseil.ConseilOperation
import io.camlcase.kotlintezos.model.conseil.ConseilTransaction
import io.camlcase.kotlintezos.model.operation.OperationType
import org.junit.Test

class ConseilClientRealTest {
    private val client =
        ConseilClient(ConseilClient.CONSEIL_TESTNET_URL, TezosNetwork.CARTHAGENET, TEST_API_KEY, true)

    private fun testGet_ReceivedOperations(
        source: Address = testWallet1!!.mainAddress,
        type: List<OperationType> = listOf(OperationType.TRANSACTION)
    ) {
        client.operationsReceived(source, types = type, callback = object : TezosCallback<List<ConseilOperation>> {
            override fun onSuccess(item: List<ConseilOperation>?) {
                println("** SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                error.printStackTrace()
                println("** ERROR! $error")
            }
        })
    }

    private fun testGet_SentOperations(
        source: Address = testWallet1!!.mainAddress,
        type: List<OperationType> = listOf(OperationType.TRANSACTION)
    ) {
        client.operationsSent(source, types = type, callback = object : TezosCallback<List<ConseilOperation>> {
            override fun onSuccess(item: List<ConseilOperation>?) {
                println("** SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                error.printStackTrace()
                println("** ERROR! $error")
            }
        })
    }

    private fun testGet_Transactions(
        source: Address = testWallet1!!.mainAddress
    ) {
        client.transactions(source, callback = object : TezosCallback<List<ConseilTransaction>> {
            override fun onSuccess(item: List<ConseilTransaction>?) {
                println("** SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                error.printStackTrace()
                println("** ERROR! $error")
            }
        })
    }


    private fun testGet_CalledOperations(
        source: Address = testWallet1!!.mainAddress,
        type: List<OperationType> = listOf(OperationType.TRANSACTION)
    ) {
        client.operationsCalled(source, types = type, callback = object : TezosCallback<List<ConseilOperation>> {
            override fun onSuccess(item: List<ConseilOperation>?) {
                println("** SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                error.printStackTrace()
                println("** ERROR! $error")
            }
        })
    }

    private fun testGet_Operations(
        source: Address = testWallet1!!.mainAddress
    ) {
        client.operations(
            source,
            sentTypes = listOf(OperationType.TRANSACTION, OperationType.DELEGATION),
            callback = object : TezosCallback<List<ConseilOperation>> {
                override fun onSuccess(item: List<ConseilOperation>?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    error.printStackTrace()
                    println("** ERROR! $error")
                }
            })
    }

    /**
     * Test against a real node.
     * Run with debugger.
     */
    @Test
    fun testRealNode() {

        // Let the connection return something
//        Thread.sleep(15 * 60 * 1000)
    }

    companion object {
        private const val TEST_API_KEY = ""
    }
}
