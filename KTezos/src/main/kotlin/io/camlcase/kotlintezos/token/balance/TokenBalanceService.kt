/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.token.balance

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.core.iterate
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.operation.metadata.MetadataService
import io.camlcase.kotlintezos.smartcontract.MichelineParameter
import io.camlcase.kotlintezos.smartcontract.convertToMicheline
import io.camlcase.kotlintezos.smartcontract.michelson.MichelineMichelsonParameter
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import io.github.vjames19.futures.jdk8.recover
import java.math.BigInteger
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * The FA1.2 standard do not define a storage structure because some of these values may be calculated upon query
 * and each implementation of FA1.2 may choose how they store or calculate data. Instead there is a series of
 * view entrypoints. All FA1.2 contracts need to expose the data via the view entrypoints in the same way.
 *
 * While view entrypoints are intended to be called another contract and used on-chain, we want to call them off-chain.
 * This can be acheived by calling a generic intermediate contract and performing a dry run that fails and returns an
 * error containing the value we want.
 *
 * @see https://gitlab.com/camlcase-dev/dexter-integration/-/blob/master/call_fa1.2_view_entrypoints.md
 */
class TokenBalanceService(
    private val network: TezosNetwork,
    private val metadataService: MetadataService,
    private val simulationService: SimulationService,
    private val executorService: ExecutorService = Executors.newCachedThreadPool()
) {

    /**
     * @param tezosNetwork Mainnet or testnet
     * @param networkClient To build the [metadataService] and [simulationService]
     * @param executorService to manage network requests and calculations asynchronously.
     */
    constructor(
        tezosNetwork: TezosNetwork,
        networkClient: NetworkClient,
        executorService: ExecutorService = Executors.newCachedThreadPool()
    ) : this(
        tezosNetwork,
        BlockchainMetadataService(networkClient, executorService),
        SimulationService(networkClient, executorService),
        executorService
    )

    /**
     * A generic smart contract that helps query the balance.
     *
     * [See docs](https://gitlab.com/camlcase-dev/dexter-integration/-/blob/master/contract-view.tz)
     */
    private val contractViewAddress: Address by lazy {
        when (network) {
            TezosNetwork.MAINNET -> BALANCE_VIEW_CONTRACT_MAINNET
            TezosNetwork.CARTHAGENET -> BALANCE_VIEW_CONTRACT_CARTHAGENET
            TezosNetwork.DELPHINET -> BALANCE_VIEW_CONTRACT_DELPHINET
            TezosNetwork.EDONET -> BALANCE_VIEW_CONTRACT_EDONET
            TezosNetwork.FLORENCENET -> BALANCE_VIEW_CONTRACT_FLORENCENET
            else -> BALANCE_VIEW_CONTRACT_FLORENCENET
        }
    }

    /**
     * Retrieves the amount of FA1.2 token ([tokenContractAddress]) that an [address] holds.
     *
     * @param tokenContractAddress KT1 address for the FA1.2 token
     * @param signatureProvider Provides the public key in case a reveal needs to be attached to the dry-run. See [revealSignature]
     * @param callback with BigInteger value.
     * FA1.2 tokens vary on decimal places. It's responsibility of the receiver to parse the integer value to its appropriate decimal representation.
     */
    fun getTokenBalance(
        tokenContractAddress: Address,
        address: Address,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<BigInteger>
    ) {
        getTokenBalance(
            tokenContractAddress,
            address,
            signatureProvider
        ).onComplete(
            onFailure = { throwable ->
                callback.onFailure(throwable.wrap())
            },
            onSuccess = { result ->
                callback.onSuccess(result ?: BigInteger.ZERO)
            })
    }

    /**
     * Retrieves the amount of FA1.2 token ([tokenContractAddress]) that an [address] holds.
     *
     * @param tokenContractAddress KT1 address for the FA1.2 token
     * @param signatureProvider Provides the public key in case a reveal needs to be attached to the dry-run. See [revealSignature]
     * @param callback with BigInteger value.
     * FA1.2 tokens vary on decimal places. It's responsibility of the receiver to parse the integer value to its appropriate decimal representation.
     */
    fun getTokenBalance(
        tokenContractAddress: Address,
        address: Address,
        signatureProvider: SignatureProvider
    ): CompletableFuture<BigInteger?> {
        return metadataService.getMetadata(address)
            .flatMap(executorService) {
                val operation = SmartContractCallOperation(
                    Tez.zero,
                    address,
                    contractViewAddress,
                    OperationFees.simulationFees,
                    MichelineMichelsonParameter(
                        getOperationMicheline(
                            tokenContractAddress,
                            address
                        )
                    )
                )
                getTokenBalance(operation, it, address, signatureProvider)
            }

    }

    /**
     * Given a list of [tokenAddresses], loops and simulates the operation that will fetch their balances. If there's
     * an error when querying any of them, it will just return the error.
     *
     * @param signatureProvider Provides the public key in case a reveal needs to be attached to the dry-run. See [revealSignature]
     * @param callback with an unordered <FA1.2 token address, FA1.2 token balance> map with no repeats
     */
    fun getTokenBalances(
        tokenAddresses: List<Address>,
        address: Address,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<HashMap<Address, BigInteger>>
    ) {
        getTokenBalances(
            tokenAddresses, address, signatureProvider
        ).onComplete(
            onFailure = { throwable ->
                callback.onFailure(throwable.wrap())
            },
            onSuccess = { result ->
                callback.onSuccess(result)
            })
    }

    /**
     * Given a list of [tokenAddresses], loops and simulates the operation that will fetch their balances. If there's
     * an error when querying any of them, it will just return the error.
     *
     * @param signatureProvider Provides the public key in case a reveal needs to be attached to the dry-run. See [revealSignature]
     * @return with an unordered <FA1.2 token address, FA1.2 token balance> map with no repeats
     */
    fun getTokenBalances(
        tokenAddresses: List<Address>,
        address: Address,
        signatureProvider: SignatureProvider
    ): CompletableFuture<HashMap<Address, BigInteger>> {
        // Get metadata
        return metadataService.getMetadata(address)
            // Since the operations are failed by default, we need to run them separately
            .flatMap(executorService) {
                val futures = tokenAddresses.map { tokenContractAddress ->
                    Future(executorService) {
                        SmartContractCallOperation(
                            Tez.zero,
                            address,
                            contractViewAddress,
                            OperationFees.simulationFees,
                            MichelineMichelsonParameter(
                                getOperationMicheline(
                                    tokenContractAddress,
                                    address
                                )
                            )
                        )
                    }.flatMap(executorService) { operation ->
                        getTokenBalance(
                            operation,
                            it,
                            address,
                            signatureProvider
                        )
                    }
                        .map(executorService) {
                            Pair(tokenContractAddress, it ?: BigInteger.ZERO)
                        }
                }
                Future.allAsList(futures.asIterable(), executorService)
            }.map(executorService) { list ->
                val map = HashMap<Address, BigInteger>(list.size)
                list.forEach { map[it.first] = it.second }
                map
            }
    }

    /**
     * Runs a dry-run of an [operation] to force an error that will contain the balance that an [address] holds.
     *
     * @return Future with BigInteger value.
     * FA1.2 tokens vary on decimal places. It's responsibility of the receiver to parse the integer value to its appropriate decimal representation.
     * @throws TezosError Any error that's not related to balance
     */
    private fun getTokenBalance(
        operation: SmartContractCallOperation,
        metadata: BlockchainMetadata,
        source: Address,
        signatureProvider: SignatureProvider
    ): CompletableFuture<BigInteger?> {
        fun TezosError?.parseBalanceError(): BigInteger? {
            val balance =
                this?.rpcErrors?.firstOrNull { it.contractArgs != null }
                    ?.contractArgs
                    ?.iterate("int")
            if (balance != null && balance is String) {
                return BigInteger(balance)
            }

            // Dry-run operations with zero XTZ balance will give an INSUFFICIENT_XTZ_BALANCE error
            val zeroBalance = this.isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)
            if (this?.type == TezosErrorType.FAILED_OPERATION || zeroBalance) {
                return BigInteger.ZERO
            }
            return null
        }

        return simulationService.simulate(listOf(operation), source, metadata, signatureProvider)
            .map(executorService) {
                if (it.errors.isNullOrEmpty()) {
                    // The dry-run should always return an error, which will have the balance side loaded.
                    throw TezosError(
                        TezosErrorType.INTERNAL_ERROR,
                        exception = IllegalStateException("There was a problem retrieving balance")
                    )

                } else {
                    val balance =
                        TezosError(TezosErrorType.FAILED_OPERATION, it.errors).parseBalanceError()
                    balance
                }
            }
            .recover {
                val balance = (it as? TezosError)?.parseBalanceError()
                balance ?: throw it
            }
    }

    private fun getOperationMicheline(
        tokenContractAddress: Address,
        address: Address
    ): MichelineParameter {
        val argMicheline = String.format(
            BALANCE_VALUE_MICHELINE,
            address,
            tokenContractAddress,
            contractViewAddress
        )
        return argMicheline.convertToMicheline() ?: emptyList()
    }

    companion object {
        private const val BALANCE_VIEW_CONTRACT_CARTHAGENET = "KT1Njyz94x2pNJGh5uMhKj24VB9JsGCdkySN"
        private const val BALANCE_VIEW_CONTRACT_DELPHINET = "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR"
        private const val BALANCE_VIEW_CONTRACT_EDONET = "KT1Hw1RXhADjcM246DjeLtQMMkGwFZ3hV1ww"
        private const val BALANCE_VIEW_CONTRACT_FLORENCENET = "KT1Nj7Jd24Dfyzfcxy286CZtuCPE5KgsZLQG"

        private const val BALANCE_VIEW_CONTRACT_MAINNET = "KT1CPuTzwC7h7uLXd5WQmpMFso1HxrLBUtpE"

        /**
         * When address is unrevealed we need to add [REVEAL] operation with the [publicKey].
         * Since this is a simulation, we avoid passing a real [SignatureProvider].
         */
        fun revealSignature(publicKey: PublicKey) = object : SignatureProvider {
            override val publicKey: PublicKey
                get() = publicKey

            override fun sign(hex: String): ByteArray? {
                return null
            }
        }

        /**
         * Micheline value for a Smart Contract call which will return the amount of a certain FA1.2 token a person has.
         * - First %s: Balance owner
         * - Second %s: Token address
         * - Third $s: Contract view address.
         */
        private const val BALANCE_VALUE_MICHELINE =
            "[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]},{\"prim\":\"NONE\",\"args\":[{\"prim\":\"key_hash\"}]},{\"prim\":\"CREATE_CONTRACT\",\"args\":[[{\"prim\":\"parameter\",\"args\":[{\"prim\":\"nat\"}]},{\"prim\":\"storage\",\"args\":[{\"prim\":\"unit\"}]},{\"prim\":\"code\",\"args\":[[{\"prim\":\"FAILWITH\"}]]}]]},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"LAMBDA\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"address\"},{\"prim\":\"unit\"}]},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"unit\"}]},[{\"prim\":\"CAR\"},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"nat\"}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"a\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"PAIR\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"address\"},{\"prim\":\"contract\",\"args\":[{\"prim\":\"nat\"}]}]}],\"annots\":[\"%%getBalance\"]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"b\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]}]]},{\"prim\":\"TRANSFER_TOKENS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"NIL\",\"args\":[{\"prim\":\"operation\"}]}]]},{\"prim\":\"CONS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"UNIT\"}]]},{\"prim\":\"PAIR\"}]]}]]},{\"prim\":\"APPLY\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"address\"},{\"string\":\"%s\"}]},{\"prim\":\"CONTRACT\",\"args\":[{\"prim\":\"lambda\",\"args\":[{\"prim\":\"unit\"},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"unit\"}]}]}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"c\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"mutez\"},{\"int\":\"0\"}]}]]},{\"prim\":\"TRANSFER_TOKENS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"NIL\",\"args\":[{\"prim\":\"operation\"}]}]]},{\"prim\":\"CONS\"}]]},{\"prim\":\"CONS\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"UNIT\"}]]},{\"prim\":\"PAIR\"}]"
    }
}
