/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos

import io.camlcase.kotlintezos.core.ext.badArgumentsError
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.data.GetBalanceRPC
import io.camlcase.kotlintezos.data.GetBigMapValueRPC
import io.camlcase.kotlintezos.data.GetContractStorageRPC
import io.camlcase.kotlintezos.data.GetDelegateRPC
import io.camlcase.kotlintezos.data.InjectOperationRPC
import io.camlcase.kotlintezos.data.blockchain.GetAddressManagerKeyRPC
import io.camlcase.kotlintezos.data.blockchain.GetChainHeadHashRPC
import io.camlcase.kotlintezos.data.blockchain.GetChainHeadRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OriginationOperationResult
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.ActivateAccountOperation
import io.camlcase.kotlintezos.model.operation.BundledFees
import io.camlcase.kotlintezos.model.operation.ForgeResponse
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationParams
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.SigningResponse
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.operation.ForgingPolicy
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.PreapplyService
import io.camlcase.kotlintezos.operation.RevealService
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.operation.fees.OperationFeesFactory
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.operation.metadata.MetadataService
import io.camlcase.kotlintezos.smartcontract.SmartContractClient
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.wallet.SigningService.sign
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Main entrypoint and provider of operations to do on the Tezos Network.
 *
 * @param networkClient implementation for connection. By default, a [DefaultNetworkClient]
 * @param executorService to manage network requests and calculations asynchronously.
 * @param metadataService Retrieves metadata for operations
 * @param forgingService Service which forges operations.
 */
class TezosNodeClient(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService,
    private val metadataService: MetadataService,
    private val forgingService: ForgingService
) : SmartContractClient {

    /**
     * Simulates operations to estimate fees
     */
    private val simulationService: SimulationService

    /**
     * A service that preapplies operations.
     */
    private val preapplyService: PreapplyService

    /**
     * Calculates fees for operations
     */
    private val operationFeesFactory: OperationFeesFactory

    constructor(
        client: NetworkClient,
        verifierClient: NetworkClient,
        executorService: ExecutorService = Executors.newCachedThreadPool()
    ) : this(
        client,
        executorService,
        BlockchainMetadataService(client, executorService),
        ForgingService(client, verifierClient, executorService)
    )

    init {
        simulationService = SimulationService(networkClient, executorService)
        preapplyService = PreapplyService(networkClient)
        operationFeesFactory =
            OperationFeesFactory(executorService, simulationService, forgingService)
    }

    /**
     * Call this when finishing to avoid leaks.
     */
    fun clear() {
        /**
         * An ExecutorService should be shut down once it is no longer needed to free up system resources and to allow graceful application shutdown.
         */
        executorService.shutdown()
    }

    fun getHead(callback: TezosCallback<Map<String, Any?>>) {
        networkClient.send(GetChainHeadRPC(), callback)
    }

    /**
     * Retrieve the hash of the block at the head of the chain.
     */
    fun getHeadHash(callback: TezosCallback<String>) {
        networkClient.send(GetChainHeadHashRPC(), callback)
    }

    /**
     * Retrieve the address manager key for the given address.
     */
    fun getManagerKey(address: Address, callback: TezosCallback<String>) {
        networkClient.send(GetAddressManagerKeyRPC(address), callback)
    }

    /**
     * Retrieves hash info, manager key and current counter for the given [address]
     *
     * @param callback With a [BlockchainMetadata] instance with all data
     */
    fun getMetadata(address: Address, callback: TezosCallback<BlockchainMetadata>) {
        metadataService.getMetadata(address)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Retrieve the balance of [Tez] of a given [Address]
     */
    override fun getBalance(address: Address, callback: TezosCallback<Tez>) {
        networkClient.send(GetBalanceRPC(address), callback)
    }

    /**
     * Dry-run a list of operations.
     */
    override fun simulateOperations(
        operations: List<SmartContractCallOperation>,
        source: Address,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<SimulationResponse>
    ) {
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Simulate operations
            .flatMap(executorService) {
                simulationService.simulate(operations, source, it, signatureProvider)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    result?.let {
                        callback.onSuccess(it)
                    } ?: callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                })
    }

    /**
     * Transact Tezos between accounts.
     *
     * @param to The address which will receive the Tez.
     * @param from The address sending the balance.
     * @param signatureProvider which will sign the operation
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun send(
        amount: Tez,
        to: Address,
        from: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.TRANSACTION,
            from,
            OperationParams.Transaction(amount, from, to),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Originate a new account from the given account.
     *
     * @param managerAddress The address which will manage the new account.
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun originateAccount(
        managerAddress: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.ORIGINATION,
            managerAddress,
            OperationParams.Origination(managerAddress, null),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Deploy a given Micheline [contractScript] with a [managerAddress]
     *
     * @param feesPolicy A policy to apply when determining operation fees. Default is estimate fees.
     * @param callback With a [OriginationOperationResult] with operation hash and originated KT1 address
     */
    fun originateContract(
        managerAddress: Address,
        contractScript: SmartContractScript,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate(),
        callback: TezosCallback<OriginationOperationResult>
    ) {
        val type = OperationType.ORIGINATION
        val params = OperationParams.Origination(managerAddress, contractScript)
        var originatedContracts: List<String>? = null
        var metadata: BlockchainMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(managerAddress)
            // Estimate fees
            .flatMap(executorService) {
                metadata = it
                operationFeesFactory
                    .calculateFees(type, params, managerAddress, feesPolicy, it, signatureProvider)
            }
            // Create Operation with calculated fees
            .map(executorService) {
                OperationFactory.createOperation(type, params, it)
            }
            // Forge
            .flatMap(executorService) {
                forge(it, managerAddress, metadata, signatureProvider)
            }
            // Sign
            .map(executorService) {
                sign(it.operationPayload, metadata!!, it.forgeResult, signatureProvider)
            }
            // Preapply
            .flatMap(executorService) {
                preapply(it, metadata)
            }
            // Inject
            .flatMap(executorService) {
                originatedContracts = it.first
                inject(it.second)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    result?.let {
                        callback.onSuccess(OriginationOperationResult(originatedContracts, it))
                    } ?: callback.onFailure(TezosError(TezosErrorType.UNEXPECTED_RESPONSE))
                })
    }

    /**
     * Register an address as a delegate.
     *
     * @param delegate The address registering as a delegate.
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun registerDelegate(
        delegate: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.DELEGATION,
            delegate,
            OperationParams.Delegation(delegate, delegate),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Delegate the balance of an account.
     *
     * @param source The address which will delegate.
     * @param delegate The address which will receive the delegation.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source]'s tz1 Address.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun delegate(
        source: Address,
        delegate: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.DELEGATION,
            source,
            OperationParams.Delegation(source, delegate),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Clear the delegate from the given [source] address.
     *
     * If the address given is a baker, the callback will return an error with [RPCErrorCause.BAKER_CANT_DELEGATE] code.
     *
     * @param source The address which will undelegate.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source]'s tz1 Address.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun undelegate(
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.DELEGATION,
            source,
            OperationParams.Delegation(source, null),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Divulge [publicKey] to the block chain
     *
     * @param source The address which will run the reveal.
     * @param publicKey The key to divulge
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [publicKey].
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun reveal(
        source: Address,
        publicKey: PublicKey,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.REVEAL,
            source,
            OperationParams.Reveal(source, publicKey),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Call a smart contract.
     *
     * @param source The address invoking the contract.
     * @param contract The smart contract to invoke.
     * @param amount The amount of Tez to transfer with the invocation. Default is 0.
     * @param parameter An optional parameter to send to the smart contract. Default is none.
     * @param entrypoint An optional entrypoint to use for the transaction. If nil, the default entry point is used.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source].
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    override fun call(
        source: Address,
        contract: Address,
        amount: Tez,
        parameter: MichelsonParameter?,
        entrypoint: String?,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy,
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.TRANSACTION,
            source,
            OperationParams.ContractCall(amount, source, contract, parameter, entrypoint),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Simulates [operations] and returns the calculated fees, gas and storage costs. Reveal operation might be added if needed.
     *
     * @param source The address which will run the operations.
     * @param signatureProvider The object which will sign the operations if [feesPolicy] is Estimate
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With the operations with the fees bundled inside.
     */
    override fun calculateFees(
        operations: List<Operation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy,
        callback: TezosCallback<CalculatedFees>
    ) {
        var metadata: BlockchainMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Add reveal operation if needed
            .map(executorService) {
                metadata = it
                RevealService.checkRevealOperation(operations, source, it, signatureProvider)
            }
            // Estimate fees for all operations
            .flatMap(executorService) {
                operationFeesFactory.calculateFees(
                    it,
                    source,
                    feesPolicy,
                    metadata!!,
                    signatureProvider
                )
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })

    }

    /**
     * Determines fees and bundles them for every one of the [operations] given. Reveal operation might be added if needed.
     *
     * @param source The address which will run the operations.
     * @param signatureProvider The object which will sign the operations if [feesPolicy] is Estimate
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With the operations with the fees bundled inside.
     */
    fun bundleFees(
        operations: List<Operation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<BundledFees>
    ) {
        var metadata: BlockchainMetadata? = null
        var finalOperations = operations
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Add reveal operation if needed
            .map(executorService) {
                metadata = it
                RevealService.checkRevealOperation(operations, source, it, signatureProvider)
            }
            // Estimate fees for all operations
            .flatMap(executorService) {
                finalOperations = it
                operationFeesFactory.calculateFees(
                    it,
                    source,
                    feesPolicy,
                    metadata!!,
                    signatureProvider
                )
            }
            // Bundle fees in operations
            .map(executorService) {
                BundledFees(finalOperations, it)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })

    }

    /**
     * Runs a given type of operation.
     *
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun runOperation(
        type: OperationType,
        source: Address,
        params: OperationParams,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        var metadata: BlockchainMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Estimate fees
            .flatMap(executorService) {
                metadata = it
                operationFeesFactory
                    .calculateFees(type, params, source, feesPolicy, it, signatureProvider)
            }
            // Create Operation with calculated fees
            .map(executorService) {
                OperationFactory.createOperation(type, params, it)
            }
            // Forge
            .flatMap(executorService) {
                forge(it, source, metadata, signatureProvider)
            }
            // Sign
            .map(executorService) {
                sign(it.operationPayload, metadata!!, it.forgeResult, signatureProvider)
            }
            // Preapply
            .flatMap(executorService) {
                preapply(it, metadata)
            }
            // Inject
            .flatMap(executorService) {
                inject(it.second)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Runs a given list of operations.
     *
     * @param source The address which will run the operations.
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation batch was successful.
     */
    fun runOperations(
        operations: List<Operation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        var metadata: BlockchainMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Estimate fees for all operations
            .flatMap(executorService) {
                metadata = it
                operationFeesFactory.calculateFees(
                    operations,
                    source,
                    feesPolicy,
                    it,
                    signatureProvider
                )
                    .map { operations.mapIndexed { index, operation -> operation.copy(it.operationsFees[index]) } }
            }
            // Forge
            .flatMap(executorService) {
                forge(it, source, metadata, signatureProvider)
            }
            // Sign
            .map(executorService) {
                sign(it.operationPayload, metadata!!, it.forgeResult, signatureProvider)
            }
            // Preapply
            .flatMap(executorService) {
                preapply(it, metadata)
            }
            // Inject
            .flatMap(executorService) {
                inject(it.second)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Runs a given list of operations with calculated fees
     * Optional: Use [calculateFees] first. You can match the fees with the operations and run them afterwards with this method.
     *
     * @param source The address which will run the operations.
     * @param signatureProvider The object which will sign the operation.
     * @param callback With a string representing the transaction ID hash if the operation batch was successful.
     */
    fun runOperations(
        operations: List<Operation>,
        source: Address,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<String>
    ) {
        var metadata: BlockchainMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Forge
            .flatMap(executorService) {
                metadata = it
                forge(operations, source, it, signatureProvider)
            }
            // Sign
            .map(executorService) {
                sign(it.operationPayload, metadata!!, it.forgeResult, signatureProvider)
            }
            // Preapply
            .flatMap(executorService) {
                preapply(it, metadata)
            }
            // Inject
            .flatMap(executorService) {
                inject(it.second)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Activates the given [address] with its [secret].
     *
     * @param address The address which will be activated
     * @param secret 20-length Hex String
     */
    fun activateAccount(
        address: Address,
        secret: String,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<String>
    ) {
        val operations = listOf(ActivateAccountOperation(address, secret))
        runOperations(
            operations,
            address,
            signatureProvider,
            callback
        )
    }

    /**
     * Forge an operation
     *
     * @throws TezosError if [operation] or [metadata] are null
     */
    private fun forge(
        operation: Operation?,
        address: Address,
        metadata: BlockchainMetadata?,
        signatureProvider: SignatureProvider
    ): CompletableFuture<ForgeResponse> {
        if (operation == null || metadata == null) {
            throw badArgumentsError("Could not retrieve operation or metadata")
        }
        return forge(listOf(operation).asIterable(), address, metadata, signatureProvider)
    }

    private fun forge(
        operations: Iterable<Operation>,
        address: Address,
        metadata: BlockchainMetadata?,
        signatureProvider: SignatureProvider
    ): CompletableFuture<ForgeResponse> {
        if (metadata == null) {
            throw badArgumentsError("Could not retrieve operation or metadata")
        }
        val payload = OperationPayload(operations, address, metadata, signatureProvider)
        return forgingService.forge(ForgingPolicy.REMOTE, payload, metadata)
            .map { ForgeResponse(payload, it) }
    }

    /**
     * Preapply an operation
     *
     * @return Completable with signedBytes for injection
     * @throws TezosError if preapplication returns a failed state.
     */
    private fun preapply(
        response: SigningResponse,
        metadata: BlockchainMetadata?
    ): CompletableFuture<Pair<List<String>?, String>> {
        return preapplyService.preapply(response.signedProtocolOperationPayload, metadata!!)
            .map(executorService) {
                if (it?.error.isNullOrEmpty()) {
                    Pair(it?.originatedContracts, response.signedBytes)
                } else {
                    throw TezosError(TezosErrorType.PREAPPLICATION_ERROR, rpcErrors = it?.error)
                }
            }
    }

    /**
     * Inject the given hex into the remote node.
     * @param signedBytes A hex payload to inject.
     * @return Completable with the operation hash
     */
    private fun inject(signedBytes: String): CompletableFuture<String?> {
        return networkClient.send(InjectOperationRPC(signedBytes))
    }

    /**
     * Retrieve the delegate of a given [Address]
     * @return String with the address of the delegate or null if none has been set.
     */
    fun getDelegate(address: Address, callback: TezosCallback<String>) {
        networkClient.send(GetDelegateRPC(address))
            .onComplete(
                onFailure = { throwable ->
                    // RPC returns 404 if delegate has not been set
                    // TODO Change this when related issue is resolved: https://gitlab.com/tezos/tezos/-/issues/831
                    if (throwable is TezosError && throwable.code == 404) {
                        callback.onSuccess(null)
                    } else {
                        callback.onFailure(throwable.wrap())
                    }
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Retrieve the storage of a smart contract.
     * @param address The address of the smart contract to inspect.
     * @param callback Completion callback which will be called with the storage.
     */
    override fun getContractStorage(address: Address, callback: TezosCallback<Map<String, Any?>>) {
        networkClient.send(GetContractStorageRPC(address), callback)
    }

    override fun runContractOperation(
        source: Address,
        params: OperationParams.ContractCall,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy,
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.TRANSACTION,
            source,
            params,
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    override fun runContractOperations(
        operations: List<SmartContractCallOperation>,
        source: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy,
        callback: TezosCallback<String>
    ) {
        runOperations(operations, source, signatureProvider, feesPolicy, callback)
    }

    /**
     * Inspect the value of a big map in a smart contract.
     * @param address The address of a smart contract with a big map.
     * @param key The key in the big map to look up.
     */
    override fun getBigMapValue(
        address: Address,
        key: MichelsonParameter,
        type: MichelsonComparable,
        callback: TezosCallback<Map<String, Any?>>
    ) {
        networkClient.send(GetBigMapValueRPC(address, key, type), callback)
    }

    companion object {
        // MainNet
        const val MAINNET_TEZTECH_NODE = "https://rpc.tezrpc.me"
        const val MAINNET_SMARTPY_NODE = "https://mainnet.smartpy.io"

        const val TESTNET_GIGANODE_NODE = "https://testnet-tezos.giganode.io"

        // DelphiNet
        const val DELPHINET_SMARTPY_NODE = "https://delphinet.smartpy.io"
        const val DELPHINET_GIGA_NODE = "https://delphinet-tezos.giganode.io"

        // EdoNet
        const val EDONET_SMARTPY_NODE = "https://edonet.smartpy.io"
        const val EDONET_GIGA_NODE = "https://edonet-tezos.giganode.io"

        // FlorenceNet
        const val FLORENCENET_SMARTPY_NODE = "https://florencenet.smartpy.io"
        const val FLORENCENET_GIGA_NODE = "https://florence-tezos.giganode.io"
        const val FLORENCENET_ECAD_NODE = "https://api.tez.ie/rpc/florencenet"

        // GranadaNet
        const val GRANADANET_SMARTPY_NODE = "https://granadanet.smartpy.io"
        const val GRANADANET_ECAD_NODE = "https://api.tez.ie/rpc/granadanet"

        /**
         * Constructor: Initialize client with default configuration.
         *
         * @param baseNodeUrl The path to the remote node
         * @param verifierNodeUrl The path to a separate node to verify forging calls. Should be different from [baseNodeUrl].
         * @param debug Log network calls
         */
        operator fun invoke(
            baseNodeUrl: String,
            verifierNodeUrl: String,
            debug: Boolean = false
        ) = run {
            val networkClient = DefaultNetworkClient(baseNodeUrl, debug)
            val executor = Executors.newCachedThreadPool()
            val metadataService = BlockchainMetadataService(
                networkClient,
                executor
            )
            val forgingService = ForgingService(networkClient, executor, verifierNodeUrl, debug)

            TezosNodeClient(networkClient, executor, metadataService, forgingService)
        }
    }
}
