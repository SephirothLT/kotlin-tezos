/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.conseil

import io.camlcase.kotlintezos.data.Payload
import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType.Operation.EQUAL
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Arguments object to query the Conseil API
 *
 * @see https://cryptonomic.github.io/ConseilJS/#/?id=list-transactions-for-an-address
 * @see https://github.com/Cryptonomic/ConseilJS/blob/master/src/reporting/ConseilQueryBuilder.ts
 */
data class ConseilQuery(
    val predicates: List<ConseilPredicate>,
    val orderBy: ConseilSortBy,
    val limit: Int = 1
) : Payload {
    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()

            val listOfPredicates = ArrayList<Map<String, Any>>()
            for (predicate in this.predicates) {
                listOfPredicates.add(predicate.payload)
            }
            payload[ARG_PREDICATES] = listOfPredicates
            payload[ARG_ORDER_BY] = listOf(orderBy.payload)
            payload[ARG_LIMIT] = limit
            return payload
        }


    companion object {
        private const val ARG_FIELDS = "fields"
        private const val ARG_PREDICATES = "predicates"
        private const val ARG_ORDER_BY = "orderBy"
        private const val ARG_LIMIT = "limit"

    }
}

data class ConseilSortBy(
    val type: ConseilSort,
    val direction: ConseilSort.ConseilSortDirection = ConseilSort.ConseilSortDirection.DESC
) : Payload {
    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()
            payload[ConseilPredicate.FIELD] = type.name.toLowerCase(Locale.ROOT)
            payload[DIRECTION] = direction.name.toLowerCase(Locale.ROOT)
            return payload
        }

    companion object {
        private const val DIRECTION = "direction"
    }
}


data class ConseilPredicate(
    val field: ConseilPredicateType.Field,
    val set: List<String>,
    val operation: ConseilPredicateType.Operation = EQUAL,
    val inverse: Boolean = false
) : Payload {
    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()
            payload[FIELD] = this.field.name.toLowerCase(Locale.ROOT)
            payload[OPERATION] = operation.value
            payload[INVERSE] = inverse
            payload[SET] = set
            return payload
        }

    companion object {
        internal const val FIELD = "field"
        private const val OPERATION = "operation"
        private const val SET = "set"
        private const val INVERSE = "inverse"
    }
}

