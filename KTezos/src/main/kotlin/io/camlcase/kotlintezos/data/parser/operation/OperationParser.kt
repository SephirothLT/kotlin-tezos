/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.operation

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.operation.DelegationOperation
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.OriginationOperation
import io.camlcase.kotlintezos.model.operation.RevealOperation
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade
import io.camlcase.kotlintezos.wallet.crypto.Prefix
import java.util.*

/**
 * @see Operation
 */
class OperationParser : Parser<Operation> {
    override fun parse(jsonData: String): Operation? {
        val parsedMap = StringMapParser().parse(jsonData)
        return parse(parsedMap)
    }

    @Suppress("LongMethod")
    fun parse(map: Map<String, Any?>): Operation? {
        try {
            val type = OperationType.get(map["kind"] as String)
            val source = map["source"] as String

            val gasLimit = map["gas_limit"] as? String
            val storageLimit = map["storage_limit"] as? String
            val operationFees = (map["fee"] as? String)?.let {
                val tezFee = Tez(it)
                OperationFees(tezFee, gasLimit?.toInt() ?: 0, storageLimit?.toInt() ?: 0)
            } ?: OperationFees.simulationFees

           return when (type) {
                OperationType.TRANSACTION -> {
                    val amount = map["amount"] as String
                    val smartContract = map["parameters"] as? HashMap<String, Any>
                    if (smartContract == null) {
                         TransactionOperation(
                            Tez(amount),
                            source,
                            map["destination"] as String,
                            operationFees
                        )
                    } else {
                        val entrypoint = smartContract["entrypoint"] as String
                        val value = smartContract["value"] as HashMap<String, Any>
                        val parameters = MichelsonParameterParser().parse(value)
                         SmartContractCallOperation(
                            Tez(amount),
                            source,
                            map["destination"] as String,
                            operationFees,
                            parameters,
                            entrypoint
                        )
                    }
                }
                OperationType.DELEGATION -> {
                     DelegationOperation(
                        source,
                        map["delegate"] as String,
                        operationFees
                    )
                }
                OperationType.REVEAL -> {
                    val key = map["public_key"] as String
                    val decoded = EncoderFacade.decodeWithPrefix(key, Prefix.edpk)
                     RevealOperation(
                        source,
                        PublicKey(decoded),
                        operationFees
                    )
                }
                OperationType.ORIGINATION -> {
                    val script = map["script"] as HashMap<String, Any>
                    val code = script["code"] as ArrayList<Map<String, Any>>
                    val storage = script["storage"] as HashMap<String, Any>
                     OriginationOperation(
                        source,
                        operationFees,
                        SmartContractScript(code, storage)
                    )
                }
            }

        } catch (e: TezosError) {
            throw e
        } catch (e: Exception) {
            return null
        }
    }
}
