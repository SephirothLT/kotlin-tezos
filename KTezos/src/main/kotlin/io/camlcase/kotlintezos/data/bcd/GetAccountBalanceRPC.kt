/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.bcd

import io.camlcase.kotlintezos.core.ext.toLowerCaseUS
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.bcd.BCDAccountResponseParser
import io.camlcase.kotlintezos.data.parser.bcd.BCDAccountTokenBalanceResponseParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.dto.BCDAccountResponse
import io.camlcase.kotlintezos.model.bcd.dto.BCDAccountTokenBalancesResponse

/**
 * Get BCD account info
 */
class GetAccountBalanceRPC(
    network: TezosNetwork,
    address: Address
) : RPC<BCDAccountResponse>(
    endpoint = formatEndpoint(network, address, ENDPOINT_PATH),
    parser = BCDAccountResponseParser()
) {
    companion object {
        internal const val ENDPOINT_PATH = "/account/%s/%s"
    }
}

/**
 * Get account token balances. Separated from [GetAccountBalanceRPC] as an address can hold a long list of assets.
 */
class GetAccountTokenBalanceRPC(
    network: TezosNetwork,
    address: Address
) : RPC<BCDAccountTokenBalancesResponse>(
    endpoint = formatEndpoint(network, address, ENDPOINT_PATH),
    parser = BCDAccountTokenBalanceResponseParser()
) {
    companion object {
        internal const val ENDPOINT_PATH = "/account/%s/%s/token_balances"
    }
}

private fun formatEndpoint(network: TezosNetwork, address: Address, endpoint: String): String {
    // Force use on the patched Edo version
    val networkText = if (network == TezosNetwork.EDONET) {
        "edo2net"
    } else {
        network.name.toLowerCaseUS()
    }
    return String.format(endpoint, networkText, address)
}
