/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.exchange

import io.camlcase.kotlintezos.core.ext.badArgumentsError
import io.camlcase.kotlintezos.data.dexter.DexterPool
import io.camlcase.kotlintezos.data.parser.token.DexterPoolParser
import io.camlcase.kotlintezos.model.*
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.TokenOperationParams
import io.camlcase.kotlintezos.model.operation.TokenOperationType
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.smartcontract.SmartContractClient
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.map
import java.math.BigInteger
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Provides operations to communicate with the decentralized exchange (DExter) of a specific FA1.2 Token
 * @see https://gitlab.com/camlcase-dev/dexter
 *
 * @param network [TezosNetwork] to connect to. Default is [TezosNetwork.MAINNET]
 * @param client An accessor to operate with smart contracts in the blockchain. E.g.: TezosNodeClient
 * @param exchangeContractAddress The Dexter address for the token
 * @param tokenContractAddress The address to that token contract. Different from the dexter exchange address!
 * @param executorService to manage network requests and calculations asynchronously.
 */
class DexterTokenClient(
    private val client: SmartContractClient,
    private val network: TezosNetwork,
    private val exchangeContractAddress: Address,
    private val tokenContractAddress: Address,
    private val indexterService: IndexterService,
    private val executorService: ExecutorService = Executors.newCachedThreadPool()
) {

    /**
     * Calculate the fees for any of the trade [entrypoint]s.
     *
     * @param source The address making the trade.
     * @param destination The destination for the trade. By default it's the [source] address making the swap.
     * @param entrypoint Either [DexterEntrypoint.TEZ_TO_TOKEN] or [DexterEntrypoint.TOKEN_TO_TEZ]. Any other will return error.
     * @param feesPolicy By default Estimate, since we are calling smart contracts.
     * @param signatureProvider The object which will sign the operations if [feesPolicy] is Estimate
     */
    fun calculateTradeFees(
        source: Address,
        destination: Address = source,
        tokenAmount: BigInteger,
        amount: Tez,
        entrypoint: DexterEntrypoint,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate(),
        signatureProvider: SignatureProvider,
        callback: TezosCallback<CalculatedFees>
    ) {
        when (entrypoint) {
            DexterEntrypoint.TOKEN_TO_TEZ -> {
                tokenForTezOperation(
                    source,
                    destination = destination,
                    tokenAmount = tokenAmount,
                    amount = amount,
                    deadline = in30Mins()
                ) { list ->
                    client.calculateFees(list, source, signatureProvider, feesPolicy, callback)
                }

            }
            DexterEntrypoint.TEZ_TO_TOKEN -> {
                tezForTokenOperation(source, destination, amount, tokenAmount, in30Mins()) {
                    client.calculateFees(it, source, signatureProvider, feesPolicy, callback)
                }
            }
            else -> callback.onFailure(TezosError(TezosErrorType.UNKNOWN))
        }
    }

    /**
     * Get xtz and FA1.2 token liquidity pool data.
     */
    fun getLiquidityPool(
        callback: TezosCallback<DexterPool>
    ) {
        client.getContractStorage(
            exchangeContractAddress,
            object : TezosCallback<Map<String, Any?>> {
                override fun onSuccess(item: Map<String, Any?>?) {
                    item?.let {
                        val dexterPool = DexterPoolParser().parse(it)
                        callback.onSuccess(dexterPool)
                    } ?: callback.onSuccess(DexterPool.empty)
                }

                override fun onFailure(error: TezosError) {
                    callback.onFailure(error)
                }
            }
        )
    }

    /**
     * Get the total balance of the exchange in Tez.
     */
    fun getTezPool(
        callback: TezosCallback<Tez>
    ) {
        client.getBalance(exchangeContractAddress, callback)
    }

    /**
     * Get the total balance of the exchange in tokens.
     *
     * @param callback With natural value of tokens. Normalising the value with the correct decimals should be done afterwards.
     */
    fun getTokenPool(
        callback: TezosCallback<BigInteger>
    ) {
        client.getContractStorage(
            exchangeContractAddress,
            object : TezosCallback<Map<String, Any?>> {
                override fun onSuccess(item: Map<String, Any?>?) {
                    item?.let {
                        val dexterPool = DexterPoolParser().parse(it)
                        callback.onSuccess(dexterPool.tokenPool)
                    } ?: callback.onSuccess(BigInteger.ZERO)
                }

                override fun onFailure(error: TezosError) {
                    callback.onFailure(error)
                }
            }
        )
    }

    /**
     * Add liquidity to the exchange.
     */
    fun addLiquidity(
        source: Address,
        amount: Tez,
        minLiquidity: BigInteger,
        maxTokens: BigInteger,
        deadline: Date = in30Mins(),
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate(),
        signatureProvider: SignatureProvider,
        callback: TezosCallback<String>
    ) {

        indexterService.getAllowance(network, tokenContractAddress, exchangeContractAddress, source)
            .map(executorService) {
                val params = TokenOperationParams.AddLiquidity(
                    exchangeContractAddress,
                    tokenContractAddress,
                    source,
                    amount,
                    minLiquidity,
                    maxTokens,
                    it ?: BigInteger.ZERO,
                    deadline
                )
                OperationFactory.createTokenOperation(
                    TokenOperationType.ADD_LIQUIDITY,
                    params,
                    OperationFees.simulationFees
                )
            }
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("Could not create operation for AddLiquidity")
                }
                client.runContractOperations(it, source, signatureProvider, feesPolicy, callback)
            }
    }

    /**
     * Buy tokens with Tez.
     *
     * Since we are calling a smart contract, fees will be estimated.
     *
     * @param source The address making the trade.
     * @param destination The destination for the trade. By default it's the [source] address making the swap.
     * @param amount The amount of Tez to sell.
     * @param tokenAmount The minimum number of tokens to purchase.
     * @param deadline A deadline for the transaction to occur by. Default is 30 minutes from now.
     * @param feesPolicy A policy to apply when determining operation fees. Default is Estimate.
     * @param signatureProvider The object which will sign the operation.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun tradeTezForToken(
        source: Address,
        destination: Address = source,
        amount: Tez,
        tokenAmount: BigInteger,
        deadline: Date = in30Mins(),
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate(),
        signatureProvider: SignatureProvider,
        callback: TezosCallback<String>
    ) {
        tezForTokenOperation(source, destination, amount, tokenAmount, deadline) {
            client.runContractOperations(it, source, signatureProvider, feesPolicy, callback)
        }
    }

    /**
     * Buy Tez with tokens.
     *
     * @param source The address making the trade.
     * @param owner The address holding the tokens. By default it's the [source] address making the swap.
     * @param destination: The destination for the tokens. By default it's the [source] address making the swap.
     * @param tokenAmount The number of tokens to sell.
     * @param amount The minimum number of Tez to buy.
     * @param deadline A deadline for the transaction to occur by. Default is 30 minutes from now.
     * @param feesPolicy A policy to apply when determining operation fees. Default is Estimate.
     * @param signatureProvider The object which will sign the operation.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun tradeTokenForTez(
        source: Address,
        owner: Address = source,
        destination: Address = source,
        tokenAmount: BigInteger,
        amount: Tez,
        deadline: Date = in30Mins(),
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate(),
        signatureProvider: SignatureProvider,
        callback: TezosCallback<String>
    ) {
        tokenForTezOperation(source, owner, destination, tokenAmount, amount, deadline) {
            client.runContractOperations(it, source, signatureProvider, feesPolicy, callback)
        }
    }

    private fun tokenForTezOperation(
        source: Address,
        owner: Address = source,
        destination: Address,
        tokenAmount: BigInteger,
        amount: Tez,
        deadline: Date = in30Mins(),
        action: (List<SmartContractCallOperation>) -> Unit
    ) {
        indexterService.getAllowance(network, tokenContractAddress, exchangeContractAddress, source)
            .map(executorService) {
                val params = TokenOperationParams.TokenToTez(
                    exchangeContractAddress,
                    tokenContractAddress,
                    source,
                    owner,
                    destination,
                    amount,
                    tokenAmount,
                    deadline,
                    it ?: BigInteger.ZERO
                )
                OperationFactory.createTokenOperation(
                    TokenOperationType.TOKEN_TO_TEZ,
                    params,
                    OperationFees.simulationFees
                )
            }
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("Could not create operation for $destination")
                }
                action(it)
            }
    }

    private fun tezForTokenOperation(
        source: Address,
        destination: Address,
        amount: Tez,
        tokenAmount: BigInteger,
        deadline: Date = in30Mins(),
        action: (List<SmartContractCallOperation>) -> Unit
    ) {
        Future {
            val params = TokenOperationParams.TezToToken(
                exchangeContractAddress,
                source,
                destination,
                amount,
                tokenAmount,
                deadline
            )
            OperationFactory.createTokenOperation(
                TokenOperationType.TEZ_TO_TOKEN,
                params,
                OperationFees.simulationFees
            )
        }
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("Could not create operation for $exchangeContractAddress")
                }
                action(it)
            }
    }

    companion object {
        /**
         * Default deadline
         */
        fun in30Mins(): Date {
            val now = Calendar.getInstance()
            now.add(Calendar.MINUTE, 30)
            return now.time
        }
    }
}

enum class DexterEntrypoint(val entrypoint: String) {
    ADD_LIQUIDITY("addLiquidity"),
    REMOVE_LIQUIDITY("removeLiquidity"),
    TOKEN_TO_TEZ("tokenToXtz"),
    TEZ_TO_TOKEN("xtzToToken")
}
