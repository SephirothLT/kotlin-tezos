/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.exchange.calculations

import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.model.Tez
import java.math.BigDecimal
import java.math.BigInteger

/**
 * Convenience methods to preview information before doing an exchange with Dexter.
 *
 * @see https://gitlab.com/camlcase-dev/dexter-integration
 * @see https://gitlab.com/camlcase-dev/dexter/-/blob/master/dexter-frontend/src/Dexter/Dexter_AddLiquidity.re
 */
interface DexterCalculations {
    /**
     * Calculate the amount of token sold for a given XTZ input and the pool state of Dexter
     * for the dexter xtzToToken entrypoint. Liquidity fee of 3% is deducted.
     *
     * @param xtzIn [Tez] amount the sender sells to Dexter. Must be greater than zero.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTezPool]
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTokenPool]
     *
     * @return BigInteger value of FA1.2 Tokens that will be exchanged.
     */
    fun xtzToTokenTokenOutput(xtzIn: Tez, xtzPool: Tez, tokenPool: BigInteger): BigInteger

    /**
     * Calculate the exchange rate for an XTZ to Token trade including the 0.3% fee given
     * to the liquidity providers and the penalty for large trades.
     * E.g.: 1 XTZ = {getXtzToTokenExchangeRate(...)} {Token Symbol}
     *
     * @param xtzIn XTZ amount the sender sells to Dexter. Must be greater than zero. By default 1.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero.
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero.
     *
     * @return The exchange rate as a BigDecimal with a scale of 6
     */
    fun xtzToTokenExchangeRate(
        xtzIn: Tez = Tez(1.0),
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal

    /**
     * Calculate the xtzToToken market rate for a give Dexter contract. The market
     * rate is an ideal number that doesn't include fees or penalties. In practice,
     * this rate cannot be executed.
     *
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero.
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero.
     * @param decimals The number of decimals a token has. Must be greater or equal to zero.
     *
     * @return The market rate as a BigDecimal
     */
    fun xtzToTokenMarketRate(xtzPool: Tez, tokenPool: BigInteger, decimals: Int): BigDecimal

    /**
     * Calculate the xtzToToken price impact for a given Dexter contract. Price
     * impact is measure of how much a trade will alter the price.
     *
     * @param xtzIn The amount of [Tez] the sender will sell to Dexter in xtzToToken.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero.
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero.
     *
     * @return The price impact percentage as a [BigDecimal] value.
     */
    fun xtzToTokenPriceImpact(xtzIn: Tez, xtzPool: Tez, tokenPool: BigInteger): BigDecimal

    /**
     * Calculate the minimum token out to be sent to Dexter for a given max [tokenOut]
     * and the max [allowedSlippage] rate the user accepts.
     *
     * @param tokenOut As calculated by [xtzToTokenTokenOutput]. Must be greater than zero.
     * @param allowedSlippage Maximum slippage rate that a user will except for an exchange. Must be between 0.00 and 1.00.
     * @return The minimum token amount to send to the xtzToToken entrypoint.
     */
    fun xtzToTokenMinimumTokenOutput(tokenOut: BigInteger, allowedSlippage: Double): BigInteger

    /**
     * Get the amount of XTZ sold for a given token input and the pool state of Dexter
     * for the dexter tokenToXtz entrypoint. Liquidity fee of 3% is deducted.
     *
     * @param tokenIn FA1.2 Token amount the sender sells to Dexter. Must be greater than zero.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTezPool]
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTokenPool]
     *
     * @return The amount of [Tez] that Dexter will send to the address in the dexter tokenToXtz entrypoint.
     */
    fun tokenToXtzXtzOutput(tokenIn: BigInteger, xtzPool: Tez, tokenPool: BigInteger): Tez

    /**
     * Calculate the exchange rate for a token to XTZ trade including the 0.3% fee given
     * to the liquidity providers and the penalty for large trades.
     * E.g.: 1 {Token Symbol} = [tokenToXtzExchangeRate] XTZ
     *
     * @param tokenIn Token amount the sender sells to Dexter. Must be greater than zero. By default 1.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero.
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero.
     *
     * @return The exchange rate as [BigDecimal]
     */
    fun tokenToXtzExchangeRate(
        tokenIn: BigInteger = BigInteger.ONE,
        xtzPool: Tez,
        tokenPool: BigInteger
    ): BigDecimal

    /**
     * Calculate the tokenToXtz market rate for a give Dexter contract. The market
     * rate is an ideal number that doesn't include fees or penalties. In practice,
     * this rate cannot be executed.
     *
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTezPool]
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTokenPool]
     * @param decimals The number of decimals a token has. Must be greater or equal to zero.
     *
     * @return The market rate as a [BigDecimal] value.
     */
    fun tokenToXtzMarketRate(xtzPool: Tez, tokenPool: BigInteger, decimals: Int): BigDecimal

    /**
     * Calculate the tokenToXtz price impact for a give Dexter contract. Price
     * impact is measure of how much a trade will alter the price.
     *
     * @param tokenIn The amount of FA1.2 token the sender will sell to Dexter in tokenToXtz.
     * @param xtzPool XTZ amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTezPool]
     * @param tokenPool Token amount that Dexter holds. Must be greater than zero. Can be queried via [DexterTokenClient.getTokenPool]
     * @return The price impact percentage as a [BigDecimal] value.
     */
    fun tokenToXtzPriceImpact(tokenIn: BigInteger, xtzPool: Tez, tokenPool: BigInteger): BigDecimal

    /**
     * Calculate the minimum token out to be sent to dexter for a given max [xtzOut]
     * and the max [allowedSlippage] rate the user accepts.
     *
     * @param xtzOut [Tez] out as calculated by [tokenToXtzXtzOutput]. Must be greater than zero.
     * @param allowedSlippage Maximum slippage rate that a user will except for an exchange. Must be greater than 0.00 and less than 1.00.
     *
     * @return The minimum [Tez] amount to send to the tokenToXtz entrypoint.
     */
    fun tokenToXtzMinimumXtzOutput(xtzOut: Tez, allowedSlippage: Double): Tez
}
