/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.fees

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * Default fees for implicit accounts (tz1)
 */
object DefaultFeeEstimator {
    fun calculateFees(protocol: TezosProtocol, type: OperationType): OperationFees {
        return when (protocol) {
            TezosProtocol.BABYLON,
            TezosProtocol.CARTHAGE,
            TezosProtocol.DELPHI,
            TezosProtocol.EDO,
            TezosProtocol.FLORENCE,
            TezosProtocol.GRANADA, -> defaultFees(type)
        }
    }

    fun calculateFees(type: OperationType): OperationFees {
        return defaultFees(type)
    }

    fun calculateFees(protocol: TezosProtocol, operations: List<Operation>): CalculatedFees {
        val operationsFees = operations.map { calculateFees(protocol, it.type) }
        return CalculatedFees(operationsFees)
    }

    /**
     * Return default fees for protocol 005/6/7/8
     * [See docs.](https://tezos.gitlab.io/protocols/005_babylon.html#gas-cost-changes)
     */
    private fun defaultFees(type: OperationType): OperationFees {
        return when (type) {
            OperationType.TRANSACTION -> OperationFees(
                Tez(0.001_284),
                10_307,
                257
            )
            OperationType.DELEGATION -> OperationFees(
                Tez(0.001_257),
                10_000,
                0
            )
            OperationType.REVEAL -> OperationFees(
                Tez(0.001_268),
                10_000,
                0
            )
            OperationType.ORIGINATION -> OperationFees(
                Tez(0.001_265),
                10_000,
                257
            )
        }
    }
}
