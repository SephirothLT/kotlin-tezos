/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.core.ext.toCallback
import io.camlcase.kotlintezos.data.RunOperationRPC
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.model.operation.payload.SignedOperationPayload
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.wallet.SimulatedSignatureProvider
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Runs an operation with empty fees so it simulated and returns gas/storage costs.
 * RPC version of --dry-run client command.
 *
 * @param networkClient To call the RPC
 * @param executorService to manage network requests and calculations asynchronously.
 */
class SimulationService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {

    /**
     * Simulate the given operation.
     *
     * @param callback To return the [SimulationResponse] asynchronously or [TezosError]
     */
    fun simulate(
        operation: Operation,
        from: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<SimulationResponse>
    ) {
        simulate(operation, from, metadata, signatureProvider)
            .toCallback(callback)
    }

    /**
     * Simulate the given operation.
     *
     * @return Completable of a [SimulationResponse]
     * @throws [TezosError]
     */
    fun simulate(
        operation: Operation,
        from: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<SimulationResponse> {
        return simulate(listOf(operation), from, metadata, signatureProvider)
    }

    /**
     * Simulate the given [operations].
     *
     * @return Completable of a [SimulationResponse]
     * @throws [TezosError]
     */
    fun simulate(
        operations: List<Operation>,
        from: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<SimulationResponse> {
        return Future(executorService) {
            val operationPayload =
                OperationPayload(operations.asIterable(), from, metadata, signatureProvider)
            SignedOperationPayload(operationPayload, metadata.chainId, SimulatedSignatureProvider.simulationSignature)
        }.flatMap { simulateOperation(metadata.constants, it) }
            .map {
                if (it == null) {
                    throw TezosError(
                        TezosErrorType.SIMULATION_FAILED,
                        exception = IllegalArgumentException("[SimulationService] Failed to simulate for { $operations }")
                    )
                }
                it!!
            }
    }

    private fun simulateOperation(
        constants: NetworkConstants,
        signedOperationPayload: SignedOperationPayload
    ): CompletableFuture<SimulationResponse?> {
        return networkClient.send(RunOperationRPC(constants, signedOperationPayload))
    }

}
