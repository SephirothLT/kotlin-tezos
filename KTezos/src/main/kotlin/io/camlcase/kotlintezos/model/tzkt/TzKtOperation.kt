/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt

import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtOperationError

/**
 * Representation of an operation in the blockchain by TzKt. [See docs](https://api.tzkt.io/#operation/Operations_GetByHash)
 */
interface TzKtOperation {
    val kind: OperationType

    /**
     * Unique ID of the operation, stored in the TzKT indexer database
     */
    val id: String

    /**
     * The height of the block from the genesis block
     */
    val level: Int

    /**
     * Hash of the operation
     */
    val hash: String

    /**
     * The datetime at which the block is claimed to have been created (ISO 8601, e.g. 2020-02-20T02:40:57Z)
     */
    val timestamp: String

    /**
     * Block hash
     */
    val block: String?

    val status: OperationResultStatus

    /**
     * A bundle of these api fields:
     * - bakerFee: Fee to the baker, produced block, in which the operation was included (micro tez)
     * - gasLimit: A cap on the amount of gas a given operation can consume
     * - storageLimit: A cap on the amount of storage a given operation can consume
     * - storageFee: The amount of funds burned from the sender account for used the blockchain storage (micro tez)
     * - allocationFee: The amount of funds burned from the sender account for account creation (micro tez)
     */
    val fees: OperationFees?

    /**
     * List of errors provided by the node, injected the operation to the blockchain. null if there is no errors
     */
    val errors: List<TzKtOperationError>?

}

