/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation.payload

import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade.encodeSignature
import io.camlcase.kotlintezos.data.Payload

/**
 * A payload for an operation and associated signature.
 *
 * @param signature The base58 signature for the operation payload.
 */
data class SignedOperationPayload(
    val operationPayload: OperationPayload,
    val chainId: String,
    val signature: String
) : Payload {

    /**
     * Implemented specifically for forge operation.
     */
    override val payload: MutableMap<String, Any>
        get() {
            val map = HashMap<String, Any>()
            val operationPayload = operationPayload.payload
            operationPayload[PAYLOAD_ARG_SIGNATURE] = signature
            map[PAYLOAD_ARG_OPERATION] = operationPayload
            map[PAYLOAD_ARG_CHAIN_ID] = chainId
            return map
        }

    companion object {
        const val PAYLOAD_ARG_SIGNATURE = "signature"
        const val PAYLOAD_ARG_OPERATION = "operation"
        const val PAYLOAD_ARG_CHAIN_ID = "chain_id"

        operator fun invoke(operationPayload: OperationPayload, chainId: String, signature: ByteArray) = run {
            SignedOperationPayload(operationPayload, chainId, encodeSignature(signature))
        }
    }

}
