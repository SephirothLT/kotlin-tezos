/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt

import io.camlcase.kotlintezos.core.ext.toUpperCaseUS
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAccountResponse
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias

/**
 * Representation of a user account in the TzKt api.
 *
 * @param delegate Information about the current delegate of the account. null if it's not delegated
 * @param numActivations Number of account activation operations. Are used to activate accounts that were recommended allocations of tezos tokens for donations to the Tezos Foundation’s fundraiser
 * @param numMigrations Number of migration (result of the context (database) migration during a protocol update) operations, related to the account (synthetic type)
 */
@Suppress("LongParameterList")
class TzKtAccount(
        val type: TzKtAccounType,
        val address: Address,
        val publicKey: String? = null,
        val revealed: Boolean = false,
        val balance: Tez = Tez.zero,
        val delegate: TzKtAlias? = null,
        val numContracts: Int = 0,
        val numActivations: Int = 0,
        val numDelegations: Int = 0,
        val numOriginations: Int = 0,
        val numTransactions: Int = 0,
        val numReveals: Int = 0,
        val numMigrations: Int = 0,
)

/**
 * Type of accounts in TzKt api
 */
enum class TzKtAccounType {
    USER,
    DELEGATE,
    CONTRACT,
    EMPTY
}

internal fun TzKtAccountResponse.map(): TzKtAccount {
    return TzKtAccount(
            if (this.type != null) {
                TzKtAccounType.valueOf(this.type.toUpperCaseUS())
            } else {
                TzKtAccounType.EMPTY
            },
            this.address ?: "",
            this.publicKey,
            this.revealed == true,
            if (this.balance != null) {
                Tez(this.balance.toString())
            } else {
                Tez.zero
            },
            this.delegate,
            this.numContracts ?: 0,
            this.numActivations ?: 0,
            this.numDelegations ?: 0,
            this.numOriginations ?: 0,
            this.numTransactions ?: 0,
            this.numReveals ?: 0,
            this.numMigrations ?: 0,
    )
}
