/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt.dto

import io.camlcase.kotlintezos.data.parser.operation.MichelsonParameterParser
import io.camlcase.kotlintezos.model.SmartContractCall
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.tzkt.TzKtDelegationResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtOriginationResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtRevealResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtTransactionResponse
import io.camlcase.kotlintezos.model.tzkt.map
import kotlinx.serialization.Polymorphic
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

/**
 * DTO for [TzKtOperation]. Used as data layer for requests before mapping it to domain.
 */
@Polymorphic
internal interface TzKtOperationResponse {
    /**
     * Type of the operation
     */
    val type: String

    /**
     * Unique ID of the operation, stored in the TzKT indexer database
     */
    val id: String

    /**
     * The height of the block from the genesis block
     */
    val level: Int

    /**
     * Hash of the operation
     */
    val hash: String

    /**
     * The datetime at which the block is claimed to have been created (ISO 8601, e.g. 2020-02-20T02:40:57Z)
     */
    val timestamp: String

    /**
     * Block hash
     */
    val block: String?

    /**
     * Operation status (applied - an operation applied by the node and successfully added to the blockchain, failed - an operation which failed with some particular error (not enough balance, gas limit, etc), backtracked - an operation which was successful but reverted due to one of the following operations in the same operation group was failed, skipped - all operations after the failed one in an operation group)
     */
    val status: String?

    /**
     * Fee to the baker, produced block, in which the operation was included (micro tez)
     */
    val bakerFee: Int

    /**
     * A cap on the amount of gas a given operation can consume
     */
    val gasLimit: Int

    /**
     * List of errors provided by the node, injected the operation to the blockchain. null if there is no errors
     */
    val errors: List<TzKtOperationError>?
}

internal fun TzKtOperationResponse.map(): TzKtOperation {
    return when (this) {
        is TzKtTransactionResponse -> {
            this.map()
        }
        is TzKtDelegationResponse -> {
            this.map()
        }
        is TzKtOriginationResponse -> {
            this.map()
        }
        is TzKtRevealResponse -> {
            this.map()
        }
        else -> throw TezosError(
            TezosErrorType.INTERNAL_ERROR,
            exception = IllegalStateException("TzKtOperationEntity of type ${this.type} not supported")
        )
    }
}

internal fun deserializeExtraFees(storageFee: Int, allocationFee: Int): ExtraFees {
    val extras = ExtraFees()
    if (storageFee > 0) {
        extras.add(BurnFee(Tez(storageFee.toString())))
    }
    if (allocationFee > 0) {
        extras.add(AllocationFee(Tez(allocationFee.toString())))
    }
    return extras
}

/**
 * Will try to deserialize the parameters string field in an operation. Assumes there's two values, "entrypoint" and "value" with the michelson parameters.
 * It will use [MichelsonParameterParser] to deserialize the "value" into a readable [MichelsonParameter].
 *
 * If it fails, it will return the full [parametersString] as the [SmartContractCall.entrypoint]
 *
 * @return SmartContractCall with fields parsed.
 */
@Suppress("TooGenericExceptionCaught")
internal fun deserializeSmartContractCall(parametersString: String?): SmartContractCall? {
    val entrypointField = "entrypoint"
    val parametersField = "value"
    if (parametersString.isNullOrBlank() || !parametersString.contains(entrypointField)) {
        return null
    }

    return try {
        val map = (Json.parseToJsonElement(parametersString) as? JsonObject)
        when {
            map != null -> {
                val entrypoint = (map[entrypointField] as JsonPrimitive).content
                val parameters = (map[parametersField] as? JsonObject)?.let {
                    MichelsonParameterParser().parse(it)
                }

                SmartContractCall(entrypoint, parameters)
            }
            else -> SmartContractCall(parametersString, null)

        }
    } catch (e: Exception) {
        SmartContractCall(parametersString, null)
    }

}
