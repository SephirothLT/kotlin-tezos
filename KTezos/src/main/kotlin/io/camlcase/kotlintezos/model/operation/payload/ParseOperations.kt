/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation.payload

import io.camlcase.kotlintezos.data.ParseOperationRPC
import io.camlcase.kotlintezos.data.Payload

/**
 * Payload for [ParseOperationRPC]
 */
data class ParseOperations(
    val operations: List<ParsedOperation>
) : Payload {
    constructor(branch: String, operationsHash: String) : this(listOf(ParsedOperation(branch, operationsHash)))

    override val payload: MutableMap<String, Any>
        get() {
            val map = HashMap<String, Any>()
            val operationsPayload: List<Map<String, Any>> = operations.map { it.payload }
            map["operations"] = operationsPayload
            map["check_signature"] = false
            return map
        }


}

/**
 * An operation formatted as needed for parsing.
 *
 * The shell_header part indicates a block an operation is meant to apply on top of. The proto part is
 * protocol-specific and appears as a binary blob.
 *
 * @see https://tezos.gitlab.io/api/rpc.html#post-block-id-helpers-parse-operations
 */
data class ParsedOperation(
    val branch: String,
    val data: String
) : Payload {
    override val payload: MutableMap<String, Any>
        get() {
            val map = HashMap<String, Any>()
            map["branch"] = branch
            map["data"] = String.format(PARSE_WRAPPER, data)
            return map
        }

    companion object {
        private const val PARSE_WRAPPER =
            "%s00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    }
}
