/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.conseil

import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * A transaction retrieved from Conseil.
 *
 * @param destination Address that received the transaction
 * @param amount Amount transacted
 */
data class ConseilTransaction(
    override val source: Address,
    val destination: Address,
    override val status: ConseilPredicateType.OperationStatus,
    val amount: Tez,
    override val timestamp: Long,
    override val fees: OperationFees?,
    override val consumedGas: Int,
    override val blockHash: String,
    override val blockLevel: Int,
    override val operationGroupHash: String,
    override val operationId: Int,
    override val smartContractInfo: ConseilSmartContractInfo?
) : ConseilOperation {
    override val kind: OperationType = OperationType.TRANSACTION
}


/**
 * A delegation retrieved from Conseil.
 * @param delegate Set by the [source]
 */
data class ConseilDelegation(
    override val source: Address,
    val delegate: String,
    override val status: ConseilPredicateType.OperationStatus,
    override val timestamp: Long,
    override val fees: OperationFees?,
    override val consumedGas: Int,
    override val blockHash: String,
    override val blockLevel: Int,
    override val operationGroupHash: String,
    override val operationId: Int,
    override val smartContractInfo: ConseilSmartContractInfo?
) : ConseilOperation {
    override val kind: OperationType = OperationType.DELEGATION
}

/**
 * An origination retrieved from Conseil.
 * @param originatedContract Address
 * @param contractScript For originating the smart contract
 */
data class ConseilOrigination(
    override val source: Address,
    val originatedContract: Address,
    val contractScript: String,
    override val status: ConseilPredicateType.OperationStatus,
    override val timestamp: Long,
    override val fees: OperationFees?,
    override val consumedGas: Int,
    override val blockHash: String,
    override val blockLevel: Int,
    override val operationGroupHash: String,
    override val operationId: Int,
    override val smartContractInfo: ConseilSmartContractInfo?

) : ConseilOperation {
    override val kind: OperationType = OperationType.ORIGINATION
}

/**
 * A reveal retrieved from Conseil
 * @param publicKey Revealed public key
 */
data class ConseilReveal(
    override val source: Address,
    val publicKey: String,
    override val status: ConseilPredicateType.OperationStatus,
    override val timestamp: Long,
    override val fees: OperationFees?,
    override val consumedGas: Int,
    override val blockHash: String,
    override val blockLevel: Int,
    override val operationGroupHash: String,
    override val operationId: Int,
    override val smartContractInfo: ConseilSmartContractInfo?
) : ConseilOperation {
    override val kind: OperationType = OperationType.REVEAL
}
