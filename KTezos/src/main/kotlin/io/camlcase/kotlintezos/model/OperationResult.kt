/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

import io.camlcase.kotlintezos.model.operation.OperationType

/**
 * The result of a given (list of) operation
 *
 * @param operationHash String representing the transaction ID hash if the operation was successful.
 */
open class OperationResult(
    val operationHash: String
)

/**
 * The result of a (list of) origination operation
 *
 * @param originatedContracts List of originated contract KT1 addresses. If null, use [operationHash] to retrieve the info.
 */
class OriginationOperationResult(
    val originatedContracts: List<String>?,
    operationHash: String
) : OperationResult(operationHash) {
    override fun toString(): String {
        return "OriginationOperationResult(originatedContracts=$originatedContracts, operationHash=$operationHash)"
    }
}
