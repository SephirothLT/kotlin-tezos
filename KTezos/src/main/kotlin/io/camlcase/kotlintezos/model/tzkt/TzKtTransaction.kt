/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.tzkt

import io.camlcase.kotlintezos.data.parser.BigIntegerSerializer
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.SmartContractCall
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtOperationResponse
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtOperationError
import io.camlcase.kotlintezos.model.tzkt.dto.deserializeExtraFees
import io.camlcase.kotlintezos.model.tzkt.dto.deserializeSmartContractCall
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.math.BigInteger

/**
 * @param amount The transaction amount in [Tez]
 * @param sender Information about the account sent the transaction
 * @param destination Information about the target of the transaction
 * @param smartContractCall Parameters/code, passed to the target contract
 * @param hasInternals Filters transactions by presence of internal operations.
 */
@Suppress("LongParameterList")
data class TzKtTransaction(
    override val id: String,
    override val level: Int,
    override val timestamp: String,
    override val block: String?,
    override val hash: String,
    override val status: OperationResultStatus,
    override val fees: OperationFees?,
    override val errors: List<TzKtOperationError>?,
    val amount: Tez,
    val sender: TzKtAlias,
    val destination: TzKtAlias,
    val smartContractCall: SmartContractCall?,
    val hasInternals: Boolean
) : TzKtOperation {
    override val kind: OperationType = OperationType.TRANSACTION
}

/**
 * Data layer class to be mapped as [TzKtTransaction] on the domain layer
 */
@Serializable
@SerialName("TzKtTransaction")
internal data class TzKtTransactionResponse(
    override val type: String,
    override val id: String,
    override val level: Int,
    override val hash: String,
    override val timestamp: String,
    override val block: String? = null,
    override val status: String? = null,
    override val bakerFee: Int,
    override val gasLimit: Int,
    @Serializable(with = BigIntegerSerializer::class)
    val amount: BigInteger,
    val sender: TzKtAlias,
    val target: TzKtAlias = TzKtAlias.empty,
    val storageLimit: Int,
    val storageFee: Int,
    val allocationFee: Int,
    val parameters: String? = null,
    val hasInternals: Boolean? = null,
    override val errors: List<TzKtOperationError>? = null,
) : TzKtOperationResponse

internal fun TzKtTransactionResponse.map(): TzKtTransaction {
    return TzKtTransaction(
        this.id,
        this.level,
        this.timestamp,
        this.block,
        this.hash,
        OperationResultStatus.get(this.status),
        OperationFees(
            Tez(this.bakerFee.toString()),
            gasLimit,
            storageLimit,
            deserializeExtraFees(storageFee, allocationFee)
        ),
        this.errors,
        Tez(this.amount.toString()),
        this.sender,
        this.target,
        deserializeSmartContractCall(this.parameters),
        this.hasInternals == true
    )
}

