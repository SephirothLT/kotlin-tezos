/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * An operation that originates a [script] or a KT1 account].
 */
data class OriginationOperation(
    override val source: Address,
    override val fees: OperationFees,
    private val contract: SmartContractScript?
) : BaseOperation {
    override val type: OperationType
        get() = OperationType.ORIGINATION

    /**
     * A map representation for the operation that matches the expected json input:
     *
     * { "kind": "origination",
     * (...)
     * "delegate"?: $Signature.Public_key_hash,
     * "script": $scripted.contracts }
     */
    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            payload[PAYLOAD_ARG_BALANCE] = "0"
            contract?.apply {
                payload[PAYLOAD_ARG_SCRIPT] = this.payload
            }
            return payload
        }

    override fun copy(newFees: OperationFees): Operation {
        return OriginationOperation(source, newFees, contract)
    }

    companion object {
        const val PAYLOAD_ARG_BALANCE = "balance"
        const val PAYLOAD_ARG_SCRIPT = "script"
    }
}
