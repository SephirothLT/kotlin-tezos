/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address

/**
 * A super interface representing an operation to perform on the blockchain.
 * Common parameters across operations and default parameter values are provided by the base class's implementation.
 */
internal interface BaseOperation : Operation {
    val source: Address
    override val requiresReveal: Boolean
        get() = when (type) {
            OperationType.DELEGATION,
            OperationType.TRANSACTION,
            OperationType.ORIGINATION -> true
            // OperationType.REVEAL
            else -> false
        }

    override val requiresCounter: Boolean
        get() = true

    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()
            payload[PAYLOAD_ARG_KIND] = type.name.toLowerCase()
            payload[PAYLOAD_ARG_SOURCE] = source
            payload[PAYLOAD_ARG_STORAGE_LIMIT] = fees.storageLimit.toString()
            payload[PAYLOAD_ARG_GAS_LIMIT] = fees.gasLimit.toString()
            payload[PAYLOAD_ARG_FEE] = fees.fee.stringRepresentation
            return payload
        }

    companion object {
        const val PAYLOAD_ARG_KIND = "kind"
        const val PAYLOAD_ARG_SOURCE = "source"
        const val PAYLOAD_ARG_STORAGE_LIMIT = "storage_limit"
        const val PAYLOAD_ARG_GAS_LIMIT = "gas_limit"
        const val PAYLOAD_ARG_FEE = "fee"
    }
}
