/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.core.js

import io.camlcase.kotlintezos.core.ext.multiLet
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import org.mozilla.javascript.Context
import org.mozilla.javascript.Function
import org.mozilla.javascript.ScriptableObject
import java.io.InputStreamReader

/**
 * Wraps functionality to call a JS file stored in the Android filesystem and execute it.
 * File needs to be in the Android /assets folder.
 *
 * Uses Rhino library to load and read the file. [See more](https://github.com/mozilla/rhino).
 * - [Source 1](https://stackoverflow.com/a/29483948/4328579)
 * - [Source 2](https://proandroiddev.com/execute-javascript-inside-android-2827cc95ad5e)
 *
 * @param androidContext To load the assets folder
 */
class JavascriptContext(
    private val androidContext: android.content.Context
) {

    private var rhinoContext: Context? = null
    private var file: ScriptableObject? = null

    /**
     * Loads the javascript context needed to interpret the file. Is called automatically when [loadFile] is called.
     * Remember to use [finish] to exit the context and avoid leaks!
     */
    fun enter() {
        if (rhinoContext == null) {
            rhinoContext = Context.enter()
        }
    }

    /**
     * Loads assets folders and inits the JS file we'll be reading.
     * If we instantiate the class once, we can reuse the loaded file for all [callFunction].
     */
    fun loadFile(fileName: String) {
        enter()
        file = callFile(fileName)
    }

    /**
     * Opens a given JS file to be read.
     *
     * @return ScriptableObject with the JS functions loaded
     */
    @Suppress("TooGenericExceptionCaught")
    private fun callFile(fileName: String): ScriptableObject? {
        var scope: ScriptableObject? = null
        rhinoContext?.apply {
            // Turn off optimization to make Rhino Android compatible
            this.optimizationLevel = -1
            scope = this.initStandardObjects()

            // Note the forth argument is 1, which means the JavaScript source has
            // been compressed to only one line using something like YUI
            val assetManager = androidContext.assets
            try {
                val input = assetManager.open(fileName)
                val targetReader = InputStreamReader(input)
                this.evaluateReader(scope, targetReader, "JavaScript", 1, null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return scope
    }

    /**
     * Executes a [functionName] with [params].
     * @throws TezosError If [loadFile] has not been called previously
     */
    fun callFunction(
        functionName: String,
        params: Array<Any> = arrayOf<Any>("")
    ): Any? {
        if (file == null) {
            throw TezosError(
                TezosErrorType.UNEXPECTED_RESPONSE,
                exception = IllegalStateException("loadFile should be called before callFunction")
            )
        }

        var jsResult: Any? = null

        rhinoContext?.apply {
            // Get the functionName defined in JavaScriptCode
            val obj = file?.get(functionName, file)
            if (obj is Function) {
                // Call the function with params
                jsResult = obj.call(rhinoContext, file, file, params)
                // Parse the jsResult object to a String
                val result: String = Context.toString(jsResult)
                return result
            }
        }
        return jsResult
    }

    /**
     * Executes a [functionName] with [params] that lives under [parentName].
     * @throws TezosError If [loadFile] has not been called previously
     */
    fun callFunction(
        parentName: String,
        functionName: String,
        params: Array<Any> = arrayOf<Any>("")
    ): String {
        if (file == null) {
            throw TezosError(
                TezosErrorType.UNEXPECTED_RESPONSE,
                exception = IllegalStateException("loadFile should be called before callFunction")
            )
        }

        var jsResult: Any? = null
        var result = ""

        multiLet(rhinoContext, file) { rhinoContext, jsFile ->
            val obj = (file!!.get(parentName) as ScriptableObject).get(functionName)
            if (obj is Function) {
                // Call the function with params
                jsResult = obj.call(rhinoContext, jsFile, jsFile, params)
                // Parse the jsResult object to a String
                result = Context.toString(jsResult)
            }
        }
        return result
    }

    /**
     * [file] is loaded at the beginning to improve calling times but [finish] should be
     * called when process is finished to avoid leaks.
     */
    fun finish() {
        Context.exit()
        rhinoContext = null
        file = null
    }
}
