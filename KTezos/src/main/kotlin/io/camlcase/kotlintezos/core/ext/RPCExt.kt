/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.core.ext

import io.camlcase.kotlintezos.core.parser.hasJsonFormat
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.RPCErrorParser
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.RPCErrorType
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

/**
 * Given a "failed" status in a results JSON, extract the "errors" into a list.
 * Uses [RPCErrorParser] with Kotlinx.serialization.
 */
fun <T> Parser<T>?.parseErrors(results: Map<*, *>): List<RPCErrorResponse> {
    this?.let {
        val list = ArrayList<RPCErrorResponse>()
        (results["errors"] as? List<*>)?.let { errors ->
            val parser = RPCErrorParser()
            for (error in errors) {
                val map = error as Map<String, Any>
                parser.parse(map)?.also {
                    list.add(it)
                }
            }
        }

        return list
    } ?: return emptyList()
}

/**
 * Given an array of mapable errors, extracts a readable list
 * Uses [RPCErrorParser] with Kotlinx.serialization.
 */
fun <T> Parser<T>?.parseErrors(array: JsonArray): List<RPCErrorResponse> {
    this?.let {
        val parser = RPCErrorParser()
        val list = ArrayList<RPCErrorResponse>()
        var i = 0
        while (i < array.size) {
            (array[i] as? JsonObject)?.let { map ->
                parser.parse(map)?.also {
                    list.add(it)
                }
            }
            i++
        }
        return list
    } ?: return emptyList()
}

/**
 * Given a json with errors such as:
 *    [
 *      {"kind":"branch","id":"proto.current_protocol.error1.description","contract":"tz1(...))"},
 *      {"kind":"branch","id":"proto.current_protocol.error2.description","contract":"tz1(...))"}
 *    ]
 *    or
 *    {"message":"Missing Authentication Token"}
 *    or
 *    "Bad connection"
 * Parses the given [jsonData] and returns a list of readable [RPCErrorResponse]
 *
 * If the message comes as a String (e.g. "Invalid key"). It will parse it and add it as cause in a [RPCErrorResponse]
 *
 * Uses Kotlinx.serialization.
 */
fun <T> Parser<T>.parseGeneralErrors(jsonData: String): List<RPCErrorResponse>? {
    fun parseErrorWith(
        string: String,
        type: RPCErrorType = RPCErrorType.PERMANENT
    ): List<RPCErrorResponse> {
        return listOf(
            RPCErrorResponse(
                type,
                string,
                contract = null
            )
        )
    }

    return if (jsonData.hasJsonFormat()) {
        val json = Json.parseToJsonElement(jsonData)
        if (json is JsonArray) {
            parseErrors(json)
        } else if (json is JsonObject) {
            if (json.contains("message")) {
                val message = (json["message"] as JsonPrimitive).content
                parseErrorWith(message, RPCErrorType.TEMPORARY)
            } else {
                null
            }
        } else if (json is JsonPrimitive) {
            parseErrorWith(json.content)
        } else {
            null
        }
    } else if (jsonData.isNotBlank()) {
        parseErrorWith(jsonData)
    } else {
        null
    }
}
