/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet.crypto

import io.camlcase.kotlintezos.wallet.crypto.Prefix.edsig

/**
 * Wrap encoding/decoding library
 */
internal object EncoderFacade {
    /**
     * Encode a Base58Check string from the given message and prefix.
     *
     * The returned address is a Base58 encoded String with the following format: [prefix][key][4 byte checksum]
     *
     * @return Empty string if [message] is empty
     */
    fun encodeCheckWithPrefix(message: ByteArray, prefix: ByteArray): String {
        val prefixedMessage = prefix + message
        val checksum = prefixedMessage.sha256HashTwice(0, prefixedMessage.size)
        return Base58.encode(prefixedMessage + checksum.sliceArray(0..3))
    }

    /**
     * Convert signature bytes to their base58 representation.
     */
    fun encodeSignature(signature: ByteArray): String {
        return encodeCheckWithPrefix(signature, edsig)
    }

    /**
     * Decode a Base58 string stripping it from prefix + checksum
     */
    fun decodeWithPrefix(prefixedMessage: String, prefix: ByteArray): ByteArray {
        val decoded = Base58.decode(prefixedMessage)
        val message = decoded.drop(prefix.size).dropLast(4)
        return message.toByteArray()
    }
}

/**
 * Common prefixes used across Tezos Cryptography.
 */
internal object Prefix {
    // Watermark
    internal val operation: ByteArray = byteArrayOf(3.toByte())

    // Keys
    internal val edpk: ByteArray = byteArrayOf(13.toByte(), 15.toByte(), 37.toByte(), 217.toByte())
    internal val edsk: ByteArray = byteArrayOf(43.toByte(), 246.toByte(), 78.toByte(), 7.toByte())

    // Forge
    internal val branch: ByteArray = byteArrayOf(1.toByte(), 52.toByte())

    // Sign
    internal val edsig: ByteArray = byteArrayOf(9.toByte(), 245.toByte(), 205.toByte(), 134.toByte(), 18.toByte())

    // Address
    internal val tz1: ByteArray = byteArrayOf(6.toByte(), 161.toByte(), 159.toByte())
    internal val kt: ByteArray = byteArrayOf(2.toByte(), 90.toByte(), 121.toByte())
}


