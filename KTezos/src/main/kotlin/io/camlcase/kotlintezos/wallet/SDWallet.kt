/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.wallet.crypto.TrustWalletFacade
import io.camlcase.kotlintezos.wallet.crypto.get32Hex

/**
 * A model of a wallet in the Tezos ecosystem using a Sequential Deterministic approach.
 *
 * @param publicKey [PublicKey] encapsulation for the Wallet
 * @param secretKey [SecretKey] encapsulation for the Wallet
 * @param mainAddress A base58check encoded public key hash for the wallet, prefixed with "tz1" which represents an address in the Tezos ecosystem.
 * @param mnemonic A list of english mnemonic words used to generate the wallet with the BIP39 specification
 */
data class SDWallet(
    override val publicKey: PublicKey,
    val secretKey: SecretKey,
    override val mainAddress: Address,
    override val mnemonic: List<String>
) : Wallet {

    /**
     * Sign the given hexadecimal encoded string with the wallet's [secretKey]
     * @param hex The string to sign.
     * @return A signature from the input.
     */
    override fun sign(hex: String): ByteArray? {
        return secretKey.sign(hex)
    }

    companion object {

        /**
         * Constructor: Create a new wallet with the given mnemonic and encrypted with an optional passphrase.
         *
         * @param mnemonic Valid list of words to generate a seed. If null or empty, it will generate one with default strength.
         * @param passphrase Extra word for security. Can be empty.
         *
         * @return The Wallet or null if error while generating mnemonic or keys.
         */
        operator fun invoke(mnemonic: List<String>? = null, passphrase: String = "") = run {
            val trustWallet = if (mnemonic.isNullOrEmpty()) {
                TrustWalletFacade().loadCoreWallet(passphrase = passphrase)
            } else {
                TrustWalletFacade().loadCoreWallet(mnemonic, passphrase)
            }
            val validatedMnemonic = trustWallet.mnemonic().split(" ")
            SDWallet(trustWallet.seed(), validatedMnemonic)

        }

        /**
         * Constructor: Create a new wallet with a seed phrase of the given [strength] and an optional passphrase.
         *
         * @param strength Must be multiple of 32 and between 128 and 256.
         *
         * @return The Wallet or null if invalid strength or error while generating mnemonic or keys.
         */
        @Suppress("MagicNumber")
        operator fun invoke(strength: Int, passphrase: String = "") = run {
            if (strength % 32 > 0 || strength !in 128..256) {
                return@run null
            }
            val trustWallet = TrustWalletFacade().loadCoreWallet(strength, passphrase)
            SDWallet(trustWallet.seed(), trustWallet.mnemonic().split(" "))
        }

        /**
         * Constructor: Create a new wallet with its seed.
         *
         * @param seed 64 byte (512 bit) array.
         * @param mnemonic The one used to generate the [seed].
         *
         * @return The Wallet or null if error while generating keys.
         */
        operator fun invoke(seed: ByteArray, mnemonic: List<String>) = run {
            val shortSeed = seed.get32Hex()
            val secretKey = SecretKey(seedString = shortSeed)
            val publicKey = PublicKey(secretKey)
            val address = publicKey?.hash
            if (secretKey == null || publicKey == null || address == null) {
                null
            } else {
                SDWallet(publicKey, secretKey, address, mnemonic)
            }
        }
    }
}
