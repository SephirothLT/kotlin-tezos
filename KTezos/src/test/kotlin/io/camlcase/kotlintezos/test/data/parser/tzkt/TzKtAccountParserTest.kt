/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.tzkt

import io.camlcase.kotlintezos.data.parser.tzkt.TzKtAccountParser
import io.camlcase.kotlintezos.model.tzkt.TzKtAccounType
import org.junit.Assert
import org.junit.Test

class TzKtAccountParserTest {

    @Test
    fun `parse Empty account`() {
        val json =
                "{\"type\":\"empty\",\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"counter\":13609509}"
        val result = TzKtAccountParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result.type == TzKtAccounType.EMPTY.name.toLowerCase())
        Assert.assertTrue(result.address != null)
        Assert.assertTrue(result.delegate == null)
        Assert.assertTrue(result.numActivations == null)
        Assert.assertTrue(result.balance == null)
    }

    @Test
    fun `parse Delegated account`() {
        val json = "{\"type\":\"user\",\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\",\"publicKey\":\"edpkutxMyctuE1adwHaSU7cLmtHDoAoajBn1Br59zgWzuc8oMJhE2N\",\"revealed\":true,\"balance\":13836979,\"counter\":8051065,\"delegate\":{\"alias\":\"Baking Tacos\",\"address\":\"tz1RV1MBbZMR68tacosb7Mwj6LkbPSUS1er1\",\"active\":true},\"delegationLevel\":1330597,\"delegationTime\":\"2021-02-04T14:09:03Z\",\"numContracts\":0,\"numActivations\":0,\"numDelegations\":2,\"numOriginations\":0,\"numTransactions\":65,\"numReveals\":1,\"numMigrations\":0,\"firstActivity\":1214151,\"firstActivityTime\":\"2020-11-13T19:17:00Z\",\"lastActivity\":1437717,\"lastActivityTime\":\"2021-04-20T21:55:44Z\"}"

        val result = TzKtAccountParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result.type == TzKtAccounType.USER.name.toLowerCase())
        Assert.assertTrue(result.address != null)
        Assert.assertTrue(result.delegate != null)
        Assert.assertTrue(result.numActivations == 0)
        Assert.assertTrue(result.balance == 13_836979L)
    }

    @Test
    fun `parse Long balance account`() {
        val json = "{\"type\":\"user\",\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\",\"publicKey\":\"edpkutxMyctuE1adwHaSU7cLmtHDoAoajBn1Br59zgWzuc8oMJhE2N\",\"revealed\":true,\"balance\":1387654329836979,\"counter\":8051065,\"numReveals\":1,\"numMigrations\":0,\"firstActivity\":1214151,\"firstActivityTime\":\"2020-11-13T19:17:00Z\",\"lastActivity\":1437717,\"lastActivityTime\":\"2021-04-20T21:55:44Z\"}"
        val result = TzKtAccountParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result.type == TzKtAccounType.USER.name.toLowerCase())
        Assert.assertTrue(result.address != null)
        Assert.assertTrue(result.delegate == null)
        Assert.assertTrue(result.numActivations == null)
        Assert.assertTrue(result.balance == 1_387_654_329_836979L)
    }
}
