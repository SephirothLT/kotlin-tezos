/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.operation

import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.data.blockchain.RPC_GET_HEAD_PATH
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.test.network.MockDispatcher
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.RPCConstants
import io.camlcase.kotlintezos.test.util.testFailure
import io.github.vjames19.futures.jdk8.onComplete
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CountDownLatch

/**
 * Integration tests suite for [BlockchainMetadataService]
 */
class BlockchainMetadataServiceTest : MockServerTest() {
    @Test
    fun testCallOK() {
        callMockDispatcher()
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        val countDownLatch = CountDownLatch(1)
        service.getMetadata("TestAddress")
            .onComplete(
                onFailure = {
                    Assert.fail("Should not call onFailure")
                    countDownLatch.countDown()
                },
                onSuccess = { result ->
                    println("*** CALLBACK onSuccess $result");
                    Assert.assertNotNull(result)
                    Assert.assertTrue(!result.blockHash.isBlank())
                    Assert.assertTrue(!result.protocol.isBlank())
                    countDownLatch.countDown()
                })
        countDownLatch.await()
    }

    @Test
    fun testCallAllKO() {
        callErrorDispatcher()
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        service.getMetadata("TestAddress")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    @Test
    fun testCallManagerKeyKO() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    else -> error404()
                }
            }
        }
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        service.getMetadata("TestAddress")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    @Test
    fun testCallCounterKO() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse()
                        .setBody("edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK")
                    else -> error404()
                }
            }
        }
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        service.getMetadata("TestAddress")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    @Test
    fun testCallHeadKO() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse()
                        .setBody("edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK")
                    else -> error404()
                }
            }
        }
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        service.getMetadata("TestAddress")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    @Test
    fun testCallHeadCrash() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("12345FAIL"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse()
                        .setBody("edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK")
                    else -> error404()
                }
            }
        }
        val service = BlockchainMetadataService(mockClient, CurrentThreadExecutor())
        service.getMetadata("TestAddress")
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

}
