package io.camlcase.kotlintezos.test.core

import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.model.*
import org.junit.Assert
import org.junit.Test

/**
 * @see RPCErrorResponseExt
 */
class RPCErrorResponseExtKtTest {

    @Test
    fun `listCauses with block error`() {
        val input = RPCErrorResponse(
            RPCErrorType.PERMANENT, "Failed to parse the request body: No case matched:\n" +
                    "      At /kind, unexpected string instead of endorsement\n" +
                    "      At /kind, unexpected string instead of seed_nonce_revelation\n" +
                    "      At /kind, unexpected string instead of double_endorsement_evidence\n" +
                    "      At /kind, unexpected string instead of double_baking_evidence\n" +
                    "      At /kind, unexpected string instead of activate_account\n" +
                    "      At /kind, unexpected string instead of proposals\n" +
                    "      At /kind, unexpected string instead of ballot\n" +
                    "      At /kind, unexpected string instead of reveal\n" +
                    "      At /destination:\n" +
                    "        Unhandled error (Failure \"Invalid contract notation.\")\n" +
                    "      At /kind, unexpected string instead of origination\n" +
                    "      At /kind, unexpected string instead of delegation"
        )

        val result = listOf(input).listCauses()

        Assert.assertEquals("Invalid contract notation.", result[0])
    }

    @Test
    fun `listCauses with block error 2`() {
        val cause = "Unhandled error (Failure \"\")\n"
        val input = RPCErrorResponse(
            RPCErrorType.PERMANENT, cause
        )

        val result = listOf(input).listCauses()

        Assert.assertEquals("$cause", result[0])
    }

    @Test
    fun `listCauses oneliner error`() {
        val input = RPCErrorResponse(
            RPCErrorType.PERMANENT, "proto.005-PsBabyM1.michelson_v1.runtime_error"
        )

        val result = listOf(input).listCauses()

        Assert.assertEquals("proto.005-PsBabyM1.michelson_v1.runtime_error", result[0])
    }

    @Test
    fun `Test parseError normal exception`() {
        val result = IllegalStateException()
            .isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)
        Assert.assertFalse(result)
    }

    @Test
    fun `isTypeOfError No RPC Errors`() {
        val result = TezosError(TezosErrorType.FAILED_OPERATION)
            .isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)
        Assert.assertFalse(result)
    }

    @Test
    fun `isTypeOfError INSUFFICIENT_FUNDS`() {
        val result = TezosError(
            TezosErrorType.RPC_ERROR, rpcErrors = listOf(
                RPCErrorResponse(
                    RPCErrorType.PERMANENT, "PSCARTHA.contract.balance_too_low.12345"
                )
            )
        ).isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)
        Assert.assertTrue(result)
    }

    @Test
    fun `isTypeOfError INVALID_CONTRACT`() {
        val result = TezosError(
            TezosErrorType.RPC_ERROR, rpcErrors = listOf(
                RPCErrorResponse(
                    RPCErrorType.PERMANENT, "PSCARTHA.invalidSyntacticConstantError.12345"
                )
            )
        ).isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
        Assert.assertTrue(result)
    }

    @Test
    fun `Test isError INVALID_CONTRACT arg1 TRUE`() {
        val input = listOf(
            RPCErrorResponse(
                RPCErrorType.PERMANENT, "Failed to parse the request body: No case matched:\n" +
                        "      At /kind, unexpected string instead of endorsement\n" +
                        "      At /kind, unexpected string instead of seed_nonce_revelation\n" +
                        "      At /kind, unexpected string instead of double_endorsement_evidence\n" +
                        "      At /kind, unexpected string instead of double_baking_evidence\n" +
                        "      At /kind, unexpected string instead of activate_account\n" +
                        "      At /kind, unexpected string instead of proposals\n" +
                        "      At /kind, unexpected string instead of ballot\n" +
                        "      At /kind, unexpected string instead of reveal\n" +
                        "      At /destination:\n" +
                        "        Unhandled error (Failure \"Invalid contract notation.\")\n" +
                        "      At /kind, unexpected string instead of origination\n" +
                        "      At /kind, unexpected string instead of delegation"
            )
        )

        val causes = input.listCauses()
        val result = causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
        Assert.assertTrue(result)
    }

    @Test
    fun `Test listCauses INVALID_CONTRACT arg2 TRUE`() {
        val input = listOf(
            RPCErrorResponse(
                RPCErrorType.PERMANENT, "PSCARTHA.invalidSyntacticConstantError.12345"
            )
        )

        val causes = input.listCauses()
        val result = causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
        Assert.assertTrue(result)
    }

    @Test
    fun `Test listCauses INVALID_CONTRACT arg3 TRUE`() {
        val input = listOf(
            RPCErrorResponse(
                RPCErrorType.PERMANENT, "PSCARTHA.contract.invalid_contract_notation.12345"
            )
        )

        val causes = input.listCauses()
        val result = causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
        Assert.assertTrue(result)
    }

    @Test
    fun `Test listCauses INVALID_CONTRACT empty FALSE`() {
        val input = listOf(
            RPCErrorResponse(RPCErrorType.PERMANENT, "")
        )

        val causes = input.listCauses()
        val result = causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
        Assert.assertFalse(result)
    }

    @Test
    fun `Test listCauses EXCHANGE_INVALID_SWAP`() {
        val input = listOf(
            RPCErrorResponse(RPCErrorType.PERMANENT, "proto.006-PsCARTHA.michelson_v1.runtime_error", null),
            RPCErrorResponse(
                RPCErrorType.PERMANENT,
                "proto.006-PsCARTHA.michelson_v1.script_rejected",
                "tokensBought is less than minTokensBought."
            )
        )

        val causes = input.listCauses()
        val result = causes.isTypeOfError(RPCErrorCause.EXCHANGE_INVALID_SWAP)
        Assert.assertTrue(result)
    }
}
