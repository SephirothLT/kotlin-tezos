/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.balance

import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.data.RunOperationRPC
import io.camlcase.kotlintezos.data.blockchain.GetNetworkConstantsRPC
import io.camlcase.kotlintezos.data.blockchain.RPC_GET_HEAD_PATH
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.test.network.MockDispatcher
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.Params.Companion.fakeAddress
import io.camlcase.kotlintezos.test.util.RPCConstants
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.token.balance.TokenBalanceService
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

/**
 * @see TokenBalanceService
 */
class TokenBalanceServiceTest : MockServerTest() {

    private fun init(): TokenBalanceService {
        val executorService = CurrentThreadExecutor()
        val metadataService = BlockchainMetadataService(mockClient, executorService)
        val simulationService = SimulationService(mockClient, executorService)
        return TokenBalanceService(
            TezosNetwork.FLORENCENET,
            metadataService,
            simulationService,
            executorService
        )
    }

    @Test
    fun `Test getTokenBalance TZG OK`() {
        val service = init()

        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse().setBody(
                        "edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK"
                    )
                    request.path?.equalsToWildcard(GetNetworkConstantsRPC.ENDPOINT) == true -> validResponse().setBody(
                        MockDispatcher.getStringFromFile("get_constants.json")
                    )
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("token_balance_run_operation_simulation.json"))
                    else -> error404()
                }
            }
        }

        service.getTokenBalance(
            TOKEN_ADDRESS,
            fakeAddress,
            fakeSignature,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertTrue(item == BigInteger("201"))
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `Test getTokenBalance zero Balance OK`() {
        val service = init()

        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse().setBody(
                        "edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK"
                    )
                    request.path?.equalsToWildcard(GetNetworkConstantsRPC.ENDPOINT) == true -> validResponse().setBody(
                        MockDispatcher.getStringFromFile("get_constants.json")
                    )
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("token_balance_zero_run_operation_simulation.json"))
                    else -> error404()
                }
            }
        }

        service.getTokenBalance(
            TOKEN_ADDRESS,
            fakeAddress,
            fakeSignature,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertTrue(item == BigInteger.ZERO)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `Test getTokenBalance Unrevealed OK`() {
        val service = init()

        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse().setBody(
                        "null"
                    )
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equalsToWildcard(GetNetworkConstantsRPC.ENDPOINT) == true -> validResponse().setBody(
                        MockDispatcher.getStringFromFile("get_constants.json")
                    )
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("token_balance_run_operation_simulation.json"))
                    else -> error404()
                }
            }
        }

        service.getTokenBalance(
            TOKEN_ADDRESS,
            fakeAddress,
            fakeSignature,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertTrue(item == BigInteger("201"))
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `Test getTokenBalance Empty XTZ KO`() {
        val service = init()

        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("get_head.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_MANAGER_KEY) == true -> validResponse().setBody(
                        "null"
                    )
                    request.path?.equalsToWildcard(RPCConstants.RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                    request.path?.equalsToWildcard(GetNetworkConstantsRPC.ENDPOINT) == true -> validResponse().setBody(
                        MockDispatcher.getStringFromFile("get_constants.json")
                    )
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody("[{\"kind\":\"branch\",\"id\":\"proto.006-PsCARTHA.implicit.empty_implicit_contract\",\"implicit\":\"tz1URhD1CtiCWHThLm5myEb7QCzJAt4R1UuA\"}]")
                    else -> error404()
                }
            }
        }

        service.getTokenBalance(
            TOKEN_ADDRESS,
            fakeAddress,
            fakeSignature,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.assertTrue(item == BigInteger("0"))
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    countDownLatch.countDown()
                    Assert.fail()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `Test getTokenBalance KO`() {
        val service = init()
        callErrorDispatcher()

        service.getTokenBalance(
            TOKEN_ADDRESS,
            fakeAddress,
            fakeSignature,
            object : TezosCallback<BigInteger> {
                override fun onSuccess(item: BigInteger?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertTrue(error.code == 400)
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    companion object {
        private const val TOKEN_ADDRESS = "KT1NisAhyEtp2pKMh5ynVPhPBfwoGFHbMe6d" // TZG
    }
}
