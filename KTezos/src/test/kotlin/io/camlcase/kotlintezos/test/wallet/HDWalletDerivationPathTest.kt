/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.wallet.HDWallet
import org.junit.Assert
import org.junit.Test

class HDWalletDerivationPathTest {

    @Test
    fun `validDerivationPath OK`() {
        val valid = listOf(
            HDWallet.DEFAULT_TEZOS_BIP44_DERIVATION_PATH,
            "m/44'/1729'/0/0/0",
            "m/44'/1729'/1\'/0\'",
            "m/44'/1729'/1\'/1",
            "m/44'/1729'/2'/0\'/1",
            "m/44'/1729'/3/0\'",
            "m/44'/1729'/489/1\'/489",
            "m/44'/1729'/12345\'/2",
        )

        valid.forEach {
            val result = HDWallet.validDerivationPath(it)
            Assert.assertTrue(result)
        }
    }

    @Test
    fun `validDerivationPath KO`() {
        val invalid = listOf(
            "m/49'/1729'/0'/0'",
            "m/44'/1728'/0'/0'",
            "m/44'/1729'/1\'/200\'",
            "kotlinTezos",
            "m/44'/1729'/2'/0\'\'/1",
            "m/44'/1729'/kotlinTezos/0\'",
            "m/44'/1729'/489/1\'/*",
            "m/44'/1729'/12345//2",
        )

        invalid.forEach {
            val result = HDWallet.validDerivationPath(it)
            Assert.assertFalse(result)
        }
    }
}
