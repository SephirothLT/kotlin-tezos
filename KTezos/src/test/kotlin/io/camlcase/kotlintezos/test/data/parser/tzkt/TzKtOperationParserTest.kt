/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser.tzkt

import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.tzkt.TzKtDelegation
import io.camlcase.kotlintezos.model.tzkt.TzKtOrigination
import io.camlcase.kotlintezos.model.tzkt.TzKtReveal
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test

class TzKtOperationParserTest {
    // Dexter swap
    @Test
    fun `parse List of 1 transaction with ERROR`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":34616631,\"level\":1190194,\"timestamp\":\"2020-10-28T00:40:28Z\",\"block\":\"BL4AgfjH9Mk6ZknQc7Ygf66CX2nbUH3E2w2mN3tZ9nnVq7zHpwu\",\"hash\":\"oo5XsmdPjxvBAbCyL9kh3x5irUmkWNwUFfi2rfiKqJGKA6Sxjzf\",\"counter\":7388570,\"sender\":{\"address\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},\"gasLimit\":613968,\"gasUsed\":0,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":61961,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"alias\":\"Dexter tzBTC/XTZ\",\"address\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\"},\"amount\":1000000,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"15308\\\"},{\\\"string\\\":\\\"2020-10-28T00:40:16Z\\\"}]}]}}\",\"status\":\"failed\",\"errors\":[{\"type\":\"michelson_v1.runtime_error\"},{\"type\":\"michelson_v1.script_rejected\"}],\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result[0] is TzKtTransaction)
        Assert.assertTrue(result[0].status == OperationResultStatus.FAILED)
        Assert.assertTrue(result[0].errors!!.size == 2)
        val tzKtTransaction = result[0] as TzKtTransaction
        // Dexter name comes with alias
        Assert.assertTrue(!tzKtTransaction.destination.alias.isNullOrBlank())
        Assert.assertTrue(tzKtTransaction.smartContractCall != null)

    }

    @Test
    fun `parse List of 1 token transaction OK`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":13197772,\"level\":844756,\"timestamp\":\"2020-11-03T14:01:46Z\",\"block\":\"BKksLHsWFWoDTH7NCUujYy4VMCXhsAHJwFAa8cpXC65mhugzcgq\",\"hash\":\"oo4an4S8NfNBGh3bP8AZiL9HoL79qu779vrVNU4cZcSYwWwLgY5\",\"counter\":553215,\"sender\":{\"address\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"},\"gasLimit\":120563,\"gasUsed\":120463,\"storageLimit\":333,\"storageUsed\":76,\"bakerFee\":12413,\"storageFee\":76000,\"allocationFee\":0,\"target\":{\"address\":\"KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\\\"},{\\\"int\\\":\\\"500000\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result[0] is TzKtTransaction)
        Assert.assertTrue(result[0].status == OperationResultStatus.APPLIED)
        val tzKtTransaction = result[0] as TzKtTransaction
        Assert.assertTrue(tzKtTransaction.smartContractCall != null)

    }

    // tokenToXTZ OK Swap
    @Test
    fun `parse List of 4 transactions OK`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":13258025,\"level\":849021,\"timestamp\":\"2020-11-05T11:22:04Z\",\"block\":\"BLeTL9jxiGd9QGSmd5Pxy555B9DKPutWHZg6Y21fL9EUy9zWsqS\",\"hash\":\"ooDd9w6qVxkPMZQEfkaYKoYo8FvmWTyhToPNSWsdPjSsdLWXQ5n\",\"counter\":3160831,\"sender\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"gasLimit\":55591,\"gasUsed\":55491,\"storageLimit\":287,\"storageUsed\":30,\"bakerFee\":7250,\"storageFee\":30000,\"allocationFee\":0,\"target\":{\"address\":\"KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"approve\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\\\"},{\\\"int\\\":\\\"50\\\"}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":13258026,\"level\":849021,\"timestamp\":\"2020-11-05T11:22:04Z\",\"block\":\"BLeTL9jxiGd9QGSmd5Pxy555B9DKPutWHZg6Y21fL9EUy9zWsqS\",\"hash\":\"ooDd9w6qVxkPMZQEfkaYKoYo8FvmWTyhToPNSWsdPjSsdLWXQ5n\",\"counter\":3160832,\"sender\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"gasLimit\":368059,\"gasUsed\":300267,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":38496,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"tokenToXtz\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\\\"},{\\\"string\\\":\\\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\\\"}]},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"50\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"2946757\\\"},{\\\"string\\\":\\\"2020-11-05T11:41:33Z\\\"}]}]}]}}\",\"status\":\"applied\",\"hasInternals\":true},{\"type\":\"transaction\",\"id\":13258027,\"level\":849021,\"timestamp\":\"2020-11-05T11:22:04Z\",\"block\":\"BLeTL9jxiGd9QGSmd5Pxy555B9DKPutWHZg6Y21fL9EUy9zWsqS\",\"hash\":\"ooDd9w6qVxkPMZQEfkaYKoYo8FvmWTyhToPNSWsdPjSsdLWXQ5n\",\"counter\":3160832,\"initiator\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"sender\":{\"address\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\"},\"nonce\":2,\"gasLimit\":0,\"gasUsed\":57485,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"0000c3dc1515f485aa362645055c2796ca6054992c9d\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"01b381697d7811f126f77945d42aae7fc9e5398f3e00\\\"},{\\\"int\\\":\\\"50\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":13258028,\"level\":849021,\"timestamp\":\"2020-11-05T11:22:04Z\",\"block\":\"BLeTL9jxiGd9QGSmd5Pxy555B9DKPutWHZg6Y21fL9EUy9zWsqS\",\"hash\":\"ooDd9w6qVxkPMZQEfkaYKoYo8FvmWTyhToPNSWsdPjSsdLWXQ5n\",\"counter\":3160832,\"initiator\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"sender\":{\"address\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\"},\"nonce\":1,\"gasLimit\":0,\"gasUsed\":10207,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"amount\":2946757,\"status\":\"applied\",\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 4)
        Assert.assertTrue(result.count { it is TzKtTransaction } == 4)
        Assert.assertTrue(result[0].status == OperationResultStatus.APPLIED)
        val tzKtTransaction = result[0] as TzKtTransaction
        Assert.assertTrue(tzKtTransaction.smartContractCall != null)
        Assert.assertTrue(tzKtTransaction.smartContractCall!!.entrypoint == "approve")

    }

    // When the operation is still on the mempool, we receive an empty list
    @Test
    fun `parse No List`() {
        val json = "[]"
        val result = TzKtOperationListParser().parse(json)
        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.isEmpty())
    }

    @Test
    fun `parse Delegation OK`() {
        val json =
            "[{\"type\":\"delegation\",\"id\":13258251,\"level\":849039,\"timestamp\":\"2020-11-05T11:31:44Z\",\"block\":\"BMcb7KsBZVtqfTaisJFBynS1Y1kDXeBjr6VVrW8q4TSrNdNggUa\",\"hash\":\"onn5PququrCmXCeeWdwHzBLQrN3q21u2589gPctsVkghqUM4ycY\",\"counter\":3160833,\"sender\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"gasLimit\":10100,\"gasUsed\":10000,\"bakerFee\":2418,\"amount\":3234469,\"newDelegate\":{\"address\":\"tz1KxFpyYzkcgq5CAok8fNvszrRcD2MUFMXy\"},\"status\":\"applied\"}]"
        val result = TzKtOperationListParser().parse(json)

        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result[0] is TzKtDelegation)
        Assert.assertTrue(result[0].status == OperationResultStatus.APPLIED)
        Assert.assertTrue(result[0].fees!!.storageLimit == 0)
        val tzKtOp = result[0] as TzKtDelegation
        Assert.assertTrue(tzKtOp.delegatedAmount == Tez("3234469"))
        Assert.assertTrue(tzKtOp.previousDelegate == null)
        Assert.assertTrue(tzKtOp.newDelegate != null)
    }

    // It has both
    //    "storageFee": 11365000,
    //    "allocationFee": 257000,
    @Test
    fun `parse Origination OK`() {
        val json =
            "[{\"type\":\"origination\",\"id\":11804240,\"level\":750155,\"timestamp\":\"2020-09-25T09:12:30Z\",\"block\":\"BMQmz9KsSVavbgKk3VuVcoC38nXrb3XYQtEC3M1bQ3q6qnUbCtw\",\"hash\":\"ooxYsRr5FYxpW6ATPewZyMkEuBkdjvao9B6d2bETv1dKZiarKmy\",\"counter\":553052,\"sender\":{\"address\":\"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV\"},\"gasLimit\":305525,\"gasUsed\":305425,\"storageLimit\":11642,\"storageUsed\":11365,\"bakerFee\":42145,\"storageFee\":11365000,\"allocationFee\":257000,\"contractBalance\":0,\"status\":\"applied\",\"originatedContract\":{\"kind\":\"smart_contract\",\"address\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\"}}]"
        val result = TzKtOperationListParser().parse(json)

        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result[0] is TzKtOrigination)
        Assert.assertTrue(result[0].status == OperationResultStatus.APPLIED)
        Assert.assertTrue(result[0].fees!!.extraFees!!.asList[0] == BurnFee(Tez("11365000")))
        Assert.assertTrue(result[0].fees!!.extraFees!!.asList[1] == AllocationFee(Tez("257000")))
        val tzKtOp = result[0] as TzKtOrigination
        Assert.assertTrue(tzKtOp.contract!!.address == "KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t")
        Assert.assertTrue(tzKtOp.timestamp == "2020-09-25T09:12:30Z")
        Assert.assertTrue(tzKtOp.hash == "ooxYsRr5FYxpW6ATPewZyMkEuBkdjvao9B6d2bETv1dKZiarKmy")
        Assert.assertTrue(tzKtOp.contractBalance == Tez.zero)
    }

    @Test
    fun `parse Reveal + Transaction`() {
        val json =
            "[{\"type\":\"reveal\",\"id\":12961526,\"level\":828029,\"timestamp\":\"2020-10-23T17:38:00Z\",\"block\":\"BMBQJnj1YAgTLreu2nQvfYnUsjWLaYJL7tFK6rm99gbNoML14SR\",\"hash\":\"oogK2caaYgK2ER92WfFR1DkfM4uyLTi5oqsBk8teEmoFo43xzU3\",\"sender\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"counter\":3160827,\"gasLimit\":10100,\"gasUsed\":10000,\"bakerFee\":1330,\"status\":\"applied\"},{\"type\":\"transaction\",\"id\":12961527,\"level\":828029,\"timestamp\":\"2020-10-23T17:38:00Z\",\"block\":\"BMBQJnj1YAgTLreu2nQvfYnUsjWLaYJL7tFK6rm99gbNoML14SR\",\"hash\":\"oogK2caaYgK2ER92WfFR1DkfM4uyLTi5oqsBk8teEmoFo43xzU3\",\"counter\":3160828,\"sender\":{\"address\":\"tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8\"},\"gasLimit\":10307,\"gasUsed\":10207,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1351,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"},\"amount\":4600000,\"status\":\"applied\",\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(json)

        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        Assert.assertTrue(result[0] is TzKtReveal)
        Assert.assertTrue(result[0].status == OperationResultStatus.APPLIED)

        val tzKtOp = result[0] as TzKtReveal
        Assert.assertTrue(tzKtOp.source!!.address == "tz1dVe77jt7cqnbuSN8Ni3uCzsMm6eMrPgJ8")

        Assert.assertTrue(result[1] is TzKtTransaction)
    }

    @Test
    fun `parse 100 operations`() {
        val input = MockDispatcher.getStringFromFile("tzkt_operations.json")
        val result = TzKtOperationListParser().parse(input)

        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 100)
        // From other people
        Assert.assertTrue(result!!.count { it is TzKtDelegation } == 11)
        Assert.assertTrue(result!!.count { it is TzKtOrigination } == 0)
        Assert.assertTrue(result!!.count { it is TzKtReveal } == 0)
        Assert.assertTrue(result!!.count { it is TzKtTransaction } == 89)
    }

    @Test
    fun `parse Transaction Big Amount`() {
        val input =
            "[{\"type\":\"transaction\",\"id\":2461290,\"level\":187542,\"timestamp\":\"2020-11-12T15:19:39Z\",\"block\":\"BMUYeDAWhKB1cLfdXJyJEMic5uh9WpWAKrmHJLHc4twx4YCwhCF\",\"hash\":\"ooX3wfW854uiRb25KiNV953BrZtfF6h5qoRrPcWbNJ5FE4J2x9q\",\"counter\":780683,\"initiator\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"sender\":{\"address\":\"KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":26023,\"storageLimit\":0,\"storageUsed\":77,\"bakerFee\":0,\"storageFee\":19250,\"allocationFee\":0,\"target\":{\"address\":\"KT1UdHaVbHEq7BUrzq2gWsC2ipsnmttV7GN7\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"011bbe9a3b596da2cb65ecd1daf1e8ce59a90202b600\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"000089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352\\\"},{\\\"int\\\":\\\"41847071\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2461289,\"level\":187542,\"timestamp\":\"2020-11-12T15:19:39Z\",\"block\":\"BMUYeDAWhKB1cLfdXJyJEMic5uh9WpWAKrmHJLHc4twx4YCwhCF\",\"hash\":\"ooX3wfW854uiRb25KiNV953BrZtfF6h5qoRrPcWbNJ5FE4J2x9q\",\"counter\":780683,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":101721,\"gasUsed\":75598,\"storageLimit\":334,\"storageUsed\":0,\"bakerFee\":11766,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS\"},\"amount\":20000000,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"41847071\\\"},{\\\"string\\\":\\\"2020-11-12T15:39:32Z\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":true},{\"type\":\"reveal\",\"id\":2461288,\"level\":187542,\"timestamp\":\"2020-11-12T15:19:39Z\",\"block\":\"BMUYeDAWhKB1cLfdXJyJEMic5uh9WpWAKrmHJLHc4twx4YCwhCF\",\"hash\":\"ooX3wfW854uiRb25KiNV953BrZtfF6h5qoRrPcWbNJ5FE4J2x9q\",\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"counter\":780682,\"gasLimit\":1100,\"gasUsed\":1000,\"bakerFee\":1687,\"status\":\"applied\"},{\"type\":\"transaction\",\"id\":2460666,\"level\":187490,\"timestamp\":\"2020-11-12T14:49:19Z\",\"block\":\"BM7uHi4R28PARU7HkWpAEKsBgUG9PZm27FvTd31t19ZDy3eY1wN\",\"hash\":\"ooKnp9xWqBkPexE98We49JAKMp6GQyKygdgiRseQffJiyXZTLLb\",\"counter\":780665,\"sender\":{\"address\":\"tz1YtiZ43hgV1gbb4B8A51YV4VYrWFVsFMKF\"},\"gasLimit\":10307,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1284,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":45000000000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2460599,\"level\":187484,\"timestamp\":\"2020-11-12T14:46:19Z\",\"block\":\"BMZtzyQ8YD8VVSaY3r1QgSbCgajLeBETnRnHFpN3MGrGUcCD4JF\",\"hash\":\"opTuAK8Jfa9HgcR7hzgsENkyZHZc6jndnYqVP7FBpkUi6yFMqvB\",\"counter\":780664,\"sender\":{\"address\":\"tz1YtiZ43hgV1gbb4B8A51YV4VYrWFVsFMKF\"},\"gasLimit\":10307,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1284,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":45000000,\"status\":\"applied\",\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(input)

        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 5)
        val tzKtOp = result[3] as TzKtTransaction
        Assert.assertTrue(tzKtOp.amount == Tez(45_000.0))
    }

    @Test
    fun `parse Delegation Big Amount`() {
        val input =
            "[{\"type\":\"delegation\",\"id\":2484287,\"level\":189563,\"timestamp\":\"2020-11-13T09:57:37Z\",\"block\":\"BMPPteXTZBxeFYW79YegLWfNdFxfWAisRvnMfwZmLos33kWPL2y\",\"hash\":\"oovrrvXS9ZGVxVzXzdbFMt52FjDgdsgow9SucE7uc9Mm3WrMGaD\",\"counter\":780684,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":10000,\"gasUsed\":1000,\"bakerFee\":1257,\"amount\":45024966040,\"newDelegate\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"status\":\"applied\"},{\"type\":\"transaction\",\"id\":2461290,\"level\":187542,\"timestamp\":\"2020-11-12T15:19:39Z\",\"block\":\"BMUYeDAWhKB1cLfdXJyJEMic5uh9WpWAKrmHJLHc4twx4YCwhCF\",\"hash\":\"ooX3wfW854uiRb25KiNV953BrZtfF6h5qoRrPcWbNJ5FE4J2x9q\",\"counter\":780683,\"initiator\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"sender\":{\"address\":\"KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":26023,\"storageLimit\":0,\"storageUsed\":77,\"bakerFee\":0,\"storageFee\":19250,\"allocationFee\":0,\"target\":{\"address\":\"KT1UdHaVbHEq7BUrzq2gWsC2ipsnmttV7GN7\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"011bbe9a3b596da2cb65ecd1daf1e8ce59a90202b600\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"000089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352\\\"},{\\\"int\\\":\\\"41847071\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2461289,\"level\":187542,\"timestamp\":\"2020-11-12T15:19:39Z\",\"block\":\"BMUYeDAWhKB1cLfdXJyJEMic5uh9WpWAKrmHJLHc4twx4YCwhCF\",\"hash\":\"ooX3wfW854uiRb25KiNV953BrZtfF6h5qoRrPcWbNJ5FE4J2x9q\",\"counter\":780683,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":101721,\"gasUsed\":75598,\"storageLimit\":334,\"storageUsed\":0,\"bakerFee\":11766,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS\"},\"amount\":20000000,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"41847071\\\"},{\\\"string\\\":\\\"2020-11-12T15:39:32Z\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":true},{\"type\":\"transaction\",\"id\":2460599,\"level\":187484,\"timestamp\":\"2020-11-12T14:46:19Z\",\"block\":\"BMZtzyQ8YD8VVSaY3r1QgSbCgajLeBETnRnHFpN3MGrGUcCD4JF\",\"hash\":\"opTuAK8Jfa9HgcR7hzgsENkyZHZc6jndnYqVP7FBpkUi6yFMqvB\",\"counter\":780664,\"sender\":{\"address\":\"tz1YtiZ43hgV1gbb4B8A51YV4VYrWFVsFMKF\"},\"gasLimit\":10307,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1284,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":45000000,\"status\":\"applied\",\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(input)

        println(result)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 4)
        val tzKtOp = result[0] as TzKtDelegation
        Assert.assertTrue(tzKtOp.delegatedAmount == Tez("45024966040"))
    }

    @Test
    fun `parse No target`() {
        val input =
            "[{\"type\":\"transaction\",\"id\":54458,\"level\":2947,\"timestamp\":\"2020-12-01T17:08:14Z\",\"block\":\"BLMCM7uj3dzmyKRbc5hh4JCDHDoSfPUJEh1FC1naAmamSu2Vjxk\",\"hash\":\"onyYjHJs4ursVijbbkoWf8gGpQMe63jR4SqoLy3tJJ8eke71VyJ\",\"counter\":768,\"sender\":{\"address\":\"tz1KhnTgwoRRALBX6vRHRnydDGSBFsWtcJxc\"},\"gasLimit\":40300,\"gasUsed\":0,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":5000,\"storageFee\":0,\"allocationFee\":0,\"amount\":100000000,\"status\":\"failed\",\"errors\":[{\"type\":\"contract.non_existing_contract\",\"contract\":\"KT1X2G6C24S91TBqDRQujCghh1NuzDwsAhqg\"}],\"hasInternals\":false}]"
        val result = TzKtOperationListParser().parse(input)

        println(result)
        Assert.assertNotNull(result)
    }

    @Test
    fun `parse FA12 token receives`() {
        val input =
            "[{\"type\":\"transaction\",\"id\":43340809,\"level\":1390225,\"timestamp\":\"2021-03-18T15:20:18Z\",\"block\":\"BLVNdpnPRELF9PFi9Kf4xB8GtQFubBaKXUZtScEbQkyRA9ViC6U\",\"hash\":\"opXZJTahBiir6tVTFPo9CjJa5tJ42FvCgn8tejP5bt5j18hWzKX\",\"counter\":8051061,\"sender\":{\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\"},\"gasLimit\":26598,\"gasUsed\":26198,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":3077,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"alias\":\"USDtz\",\"address\":\"KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"from\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\",\"value\":\"500000\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\\\"},{\\\"int\\\":\\\"500000\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":42736017,\"level\":1378410,\"timestamp\":\"2021-03-10T08:47:38Z\",\"block\":\"BMDeRVehki9UU3EWWKR1FYKgtfn397dYDb6vJdguydbHbVikBRC\",\"hash\":\"onkCFsZ8oVXA1h37ffrdf1FWrRftFtCuHbRSePn6XsUJmhQy1ZT\",\"counter\":8266266,\"initiator\":{\"address\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\"},\"sender\":{\"alias\":\"Dexter tzBTC/XTZ\",\"address\":\"KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":50033,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"alias\":\"tzBTC\",\"address\":\"KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"from\":\"KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N\",\"value\":\"3976\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"011d6f3c55ea95461dc0b4ffc14dff5c54380b24f100\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"00002a1bb751bb551a5c40b6450c4ff34c66638f3989\\\"},{\\\"int\\\":\\\"3976\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":42139998,\"level\":1367110,\"timestamp\":\"2021-03-02T11:03:15Z\",\"block\":\"BMeTQ7mRMJyyJBZcRb6GmFMQAgfo4pMr8fUpncyw5ks6b7kCWia\",\"hash\":\"oo2Jo1RMWkHGNQifkn8froy63UVUeVkzffZpdeu5KCjPBNKbSoE\",\"counter\":8051059,\"sender\":{\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\"},\"gasLimit\":26682,\"gasUsed\":26282,\"storageLimit\":334,\"storageUsed\":77,\"bakerFee\":3091,\"storageFee\":19250,\"allocationFee\":0,\"target\":{\"alias\":\"kUSD\",\"address\":\"KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"from\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\",\"value\":\"5000000000000000\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\\\"},{\\\"int\\\":\\\"5000000000000000\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":40981437,\"level\":1343543,\"timestamp\":\"2021-02-13T22:04:50Z\",\"block\":\"BKoazR9Ukv97x6QFfm5SeA1K6rmKFFd2LXRtseHT1C8B5C2bTfH\",\"hash\":\"ooxLKydsDR4DX4uFcpCvqHJRTbXE2AF4sbbGQ5dHwvyj6YHFAmT\",\"counter\":7613085,\"sender\":{\"address\":\"tz1RbQRXYrjcLyHV8nuEDLZsEGwLnJGEjoZt\"},\"gasLimit\":26598,\"gasUsed\":26198,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":3077,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"alias\":\"USDtz\",\"address\":\"KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"from\":\"tz1RbQRXYrjcLyHV8nuEDLZsEGwLnJGEjoZt\",\"value\":\"100000\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1RbQRXYrjcLyHV8nuEDLZsEGwLnJGEjoZt\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\\\"},{\\\"int\\\":\\\"100000\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":38685458,\"level\":1291962,\"timestamp\":\"2021-01-07T14:34:25Z\",\"block\":\"BLo4GRMHcWKFsCwev8SwP7nzi6xc12knSPWLTyNuXie6W1sQffk\",\"hash\":\"opKwPQvpMDvsZGCJELfRjfVzKDXdneRzqeqkcTFYmyMENkHAohE\",\"counter\":8266261,\"initiator\":{\"address\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\"},\"sender\":{\"alias\":\"Dexter ETHtz/XTZ Old\",\"address\":\"KT19c8n5mWrqpxMcR3J687yssHxotj88nGhZ\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":26032,\"storageLimit\":0,\"storageUsed\":76,\"bakerFee\":0,\"storageFee\":19000,\"allocationFee\":0,\"target\":{\"alias\":\"ETHtz\",\"address\":\"KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"from\":\"KT19c8n5mWrqpxMcR3J687yssHxotj88nGhZ\",\"value\":\"104861700067792\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"010b3a3fbab172bc1cededa318c00c4e659e05f56800\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"00002a1bb751bb551a5c40b6450c4ff34c66638f3989\\\"},{\\\"int\\\":\\\"104861700067792\\\"}]}]}}\"}]"
        val result = TzKtOperationListParser().parse(input)

        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 5)
    }

    @Test
    fun `parse Collect`() {
        val input =
            "[{\"type\":\"transaction\",\"id\":44299088,\"level\":1407060,\"timestamp\":\"2021-03-30T10:05:32Z\",\"block\":\"BLvuoYy9wpXFmRLFDQyEdmhD25be9BhPkqNyoWKKnCiywCdG74Y\",\"hash\":\"op4Ho9yemgXdUJZmAmUVifoFN2rAHj2z3k2mCpxv3Ba3SCPG49N\",\"counter\":8051063,\"initiator\":{\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\"},\"sender\":{\"address\":\"KT1Hkg5qeNhfwpKW4fXvq7HGZB9z2EnmCCA9\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":26672,\"storageLimit\":0,\"storageUsed\":67,\"bakerFee\":0,\"storageFee\":16750,\"allocationFee\":0,\"target\":{\"address\":\"KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":[{\"txs\":[{\"to_\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\",\"amount\":\"1\",\"token_id\":\"4934\"}],\"from_\":\"KT1Hkg5qeNhfwpKW4fXvq7HGZB9z2EnmCCA9\"}]},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":[{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"016498b7494a18a572c1d24484038545662c0454ed00\\\"},[{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"000066d893bbf5bd92ce081615c86f90def7794ccde6\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"4934\\\"},{\\\"int\\\":\\\"1\\\"}]}]}]]}]}\"},{\"type\":\"transaction\",\"id\":44299087,\"level\":1407060,\"timestamp\":\"2021-03-30T10:05:32Z\",\"block\":\"BLvuoYy9wpXFmRLFDQyEdmhD25be9BhPkqNyoWKKnCiywCdG74Y\",\"hash\":\"op4Ho9yemgXdUJZmAmUVifoFN2rAHj2z3k2mCpxv3Ba3SCPG49N\",\"counter\":8051063,\"sender\":{\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV6j\"},\"gasLimit\":98398,\"gasUsed\":71627,\"storageLimit\":67,\"storageUsed\":0,\"bakerFee\":10150,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1Hkg5qeNhfwpKW4fXvq7HGZB9z2EnmCCA9\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"collect\",\"value\":{\"swap_id\":\"16492\",\"objkt_amount\":\"1\"}},\"status\":\"applied\",\"hasInternals\":true,\"parameters\":\"{\\\"entrypoint\\\":\\\"collect\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"1\\\"},{\\\"int\\\":\\\"16492\\\"}]}}\"}]"
        val result = TzKtOperationListParser().parse(input)

        println(result)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        val tzKtOp = result[0] as TzKtTransaction
        Assert.assertTrue(tzKtOp.smartContractCall!!.entrypoint == "transfer")
        Assert.assertTrue(!tzKtOp.hasInternals)
        Assert.assertTrue((result[1] as TzKtTransaction).smartContractCall!!.entrypoint == "collect")
        Assert.assertTrue((result[1] as TzKtTransaction).hasInternals)
    }
}
