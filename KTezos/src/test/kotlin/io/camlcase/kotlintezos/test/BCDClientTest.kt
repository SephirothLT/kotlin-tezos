/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.data.bcd.GetAccountBalanceRPC
import io.camlcase.kotlintezos.data.bcd.GetAccountTokenBalanceRPC
import io.camlcase.kotlintezos.data.bcd.GetOperationByHashRPC
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.bcd.BCDOperation
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Test

class BCDClientTest : MockServerTest() {
    var mockHash = "ooo000"
    var mockAddress = "tz1"
    var mockNetwork = "edo2net"//TezosNetwork.DELPHINET.name.toLowerCase()
    val defaultDispatcher = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when {
                request.path?.equals(GetOperationByHashRPC.ENDPOINT_PATH + mockHash) == true ->
                    validResponse().setBody(
                        "[{\"id\":\"960e0f0734a245d7a5269f87d94196ab\",\"allocated_destination_contract_burned\": 543211,\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":false,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\",\"fee\":45252,\"counter\":2970556,\"gas_limit\":448009,\"amount\":10000000,\"destination\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"status\":\"backtracked\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.006-PsCARTHA.storage_exhausted.operation\",\"title\":\"Storage quota exceeded for the operation\",\"descr\":\"A script or one of its callee wrote more bytes than the operation said it would\",\"kind\":\"temporary\"}],\"result\":{\"consumed_gas\":328280,\"storage_size\":11835},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"2665962\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-10-06 16:01:54.194 +0000 UTC\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null},{\"id\":\"f7a2ccd1794e443f883a9e8cdd0e8c10\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"onsWvcyryBfwT7zMa6x9KShm6JKdkSNnoN87GRAgKy2VAf2k17M\",\"internal\":true,\"network\":\"carthagenet\",\"timestamp\":\"2020-10-06T15:42:22Z\",\"level\":781888,\"kind\":\"transaction\",\"source\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\",\"counter\":2970556,\"destination\":\"KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY\",\"status\":\"backtracked\",\"entrypoint\":\"transfer\",\"result\":{\"consumed_gas\":119629,\"storage_size\":5614,\"paid_storage_size_diff\":72},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"from\",\"value\":\"KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t\"},{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1RXevdVHRjueQCfLDt3sVDYyb8x51TtEfh\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"value\",\"value\":\"2672571\"}]},\"mempool\":false,\"content_index\":1,\"rawMempool\":null}]"
                    )
                request.path?.equalsToWildcard(
                    String.format(
                        GetAccountBalanceRPC.ENDPOINT_PATH,
                        mockNetwork,
                        mockAddress
                    )
                ) == true ->
                    validResponse().setBody(ACCOUNT_JSON)

                request.path?.equalsToWildcard(
                    String.format(
                        GetAccountTokenBalanceRPC.ENDPOINT_PATH,
                        mockNetwork,
                        mockAddress
                    )
                ) == true ->
                    validResponse().setBody(TOKENS_JSON)
                else -> error404()
            }
        }
    }

    @Test
    fun `getOperationDetails OK`() {
        webServer.dispatcher = defaultDispatcher
        val client = BCDClient(mockClient, CurrentThreadExecutor())
        client.getOperationDetails(mockHash, callback = object : TezosCallback<List<BCDOperation>> {
            override fun onSuccess(item: List<BCDOperation>?) {
                Assert.assertTrue(item!!.size == 2)
                countDownLatch.countDown()
            }

            override fun onFailure(error: TezosError) {
                Assert.fail()
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
    }

    @Test
    fun `getAccountBalance OK`() {
        webServer.dispatcher = defaultDispatcher
        val client = BCDClient(mockClient, CurrentThreadExecutor())
        client.getAccountBalance(
            mockAddress,
            TezosNetwork.EDONET,
            callback = object : TezosCallback<BCDAccount> {
                override fun onSuccess(item: BCDAccount?) {
                    Assert.assertTrue(item!!.address == "tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV66")
                    Assert.assertTrue(item!!.balance == Tez("14349327"))
                    Assert.assertTrue(item.tokens.size == 5)
                    Assert.assertTrue(item.tokens[0].decimals == 6)
                    Assert.assertTrue(item.tokens[2].symbol == "tzBTC")
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    @Test
    fun `getAccountBalance Account OK + Tokens KO`() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equalsToWildcard(
                        String.format(
                            GetAccountBalanceRPC.ENDPOINT_PATH,
                            mockNetwork,
                            mockAddress
                        )
                    ) == true ->
                        validResponse().setBody(
                            ACCOUNT_JSON
                        )
                    else -> error404()
                }
            }
        }
        val client = BCDClient(mockClient, CurrentThreadExecutor())
        client.getAccountBalance(
            mockAddress,
            TezosNetwork.EDONET,
            callback = object : TezosCallback<BCDAccount> {
                override fun onSuccess(item: BCDAccount?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertTrue(error.type == TezosErrorType.RPC_ERROR)
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    @Test
    fun `getAccountBalance KO`() {
        callErrorDispatcher()
        val client = BCDClient(mockClient, CurrentThreadExecutor())
        client.getAccountBalance(
            mockAddress,
            TezosNetwork.EDONET,
            callback = object : TezosCallback<BCDAccount> {
                override fun onSuccess(item: BCDAccount?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertTrue(error.code == 400)
                    Assert.assertTrue(error.type == TezosErrorType.RPC_ERROR)
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    @Test
    fun `getAccountBalance Account KO + Tokens OK`() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equalsToWildcard(
                        String.format(
                            GetAccountTokenBalanceRPC.ENDPOINT_PATH,
                            mockNetwork,
                            mockAddress
                        )
                    ) == true ->
                        validResponse().setBody(TOKENS_JSON)
                    else -> error404()
                }
            }
        }
        val client = BCDClient(mockClient, CurrentThreadExecutor())
        client.getAccountBalance(
            mockAddress,
            TezosNetwork.EDONET,
            callback = object : TezosCallback<BCDAccount> {
                override fun onSuccess(item: BCDAccount?) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertTrue(error.type == TezosErrorType.RPC_ERROR)
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    companion object {
        private const val ACCOUNT_JSON =
            "{\"address\":\"tz1V1q4j3Vd6VsPQPKecmf4pSAHNEQ6TeV66\",\"network\":\"edo2net\",\"balance\":14349327,\"tx_count\":21,\"last_action\":\"2021-03-18T15:20:18Z\"}"
        private const val TOKENS_JSON =
            "{\"balances\":[{\"contract\":\"KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"1558813\",\"decimals\":6},{\"contract\":\"KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"1340530425544773\"},{\"contract\":\"KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"4820\",\"symbol\":\"tzBTC\",\"name\":\"Wrapped BTC\"},{\"contract\":\"KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"1990000000000000000\"},{\"contract\":\"KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"1320029\"}],\"total\":5}"
    }
}
